# Bandit Level 27 → Level 28


## Question
There is a git repository at **ssh://bandit27-git@localhost/home/bandit27-git/repo**. The password for the user **bandit27-git** is the same as for the user **bandit27**.

Clone the repository and find the password for the next level.


## Solution
This was simple level. Just clone the repo in **/tmp** and you will get the password
