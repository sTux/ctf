#include <stdio.h>


char rotate(char n, char d) {
    // right rotate to emulate the ROR instruction
    return (n >> d) | (char)(n << (8 - d));
}


int main(void) {
    // First 8 bytes from the encrypted latin alphabet file and corresponding cleartext
    char cleartext[8] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};
    char encrypted[8] = {0x0F, 0xCE, 0x60, 0xBC, 0xE6, 0x2F, 0x46, 0xEA};

    // The idea is to get XOR the encrypted and the cleartext bytes to get the password
    // But for that we need to properly modify the cleartext bytes, as it would be done by the encryption algorithm
    // Only then the XOR will succeed. These steps can be re-constructed from the decryption function disassembly
    for (char i=0; i<8; i++) {
        char ch = cleartext[i] + i;
        ch = rotate(ch, i);
        ch = ch ^ encrypted[i];
        // printf("%d", "%c"\n", ch, ch);
        printf("%c", ch);
    }
    printf("\n");
    return 0;
}
