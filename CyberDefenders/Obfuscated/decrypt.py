# This script can be used to decrypt the maintool.js script from the object embedded in stream 17
# The object needs to be extracted using olevba --select=17 --extract <file> > object.bin
with open("object.bin", "rb") as fin:
    encrypted = fin.read()

encrypted_len = 16827
decrypted = ""

seed = 45
for i in range(encrypted_len):
    decrypted += chr(encrypted[i] ^ seed)
    seed = ((seed ^ 99) ^ (i % 254))

with open("maintool.js", "w") as fout:
    print(decrypted, file=fout)
