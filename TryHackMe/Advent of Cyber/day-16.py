from zipfile import ZipFile, ZipInfo, is_zipfile
from argparse import ArgumentParser
from pathlib import Path
import exiftool
from subprocess import run

def get_extract_dir(path: Path) -> Path:
    parent = path.parent
    extension = path.suffix
    name = path.name
    folder = parent.joinpath(name.replace(extension, ""))
    if not folder.is_dir():
        folder.mkdir()
    return folder


def contains_tag(file_path: Path, tag: str, value) -> bool:
    if file_path.exists() and file_path.is_file():
        file_path = str(file_path.absolute())
        tool = exiftool.ExifTool()
        tool.start()
        metadata = tool.get_metadata(file_path)
        tool.terminate()
        for cur_tag in metadata:
            if tag in cur_tag and str(metadata[cur_tag]) == value:
                return True
        return False
    else:
        return False


def extract_count(zip_file: ZipFile, inner_file: ZipInfo, folder: Path, tag: str) -> int:
    path = Path(zip_file.extract(inner_file, path=folder))
    if not (path.exists() and path.is_file() and is_zipfile(path)):
        return 0
    extract = get_extract_dir(path)
    inner_zip = ZipFile(path)
    files = inner_zip.infolist()
    tag_count = 0
    for inner_file in files:
        inner_file = inner_zip.extract(inner_file, path=extract)
        tag_count += contains_tag(Path(inner_file), tag=tag, value="1.1")

    return {
            "extracted": len(files),
            "contains_tag": tag_count
    }


def extract_examine(file_path, tag="Version"):
    folder = get_extract_dir(file_path)
    zip_file = ZipFile(file_path)
    files = zip_file.infolist()
    
    total_files = 0
    tag_count = 0
    for inner_file in files:
        result = extract_count(zip_file, inner_file, folder, tag)
        total_files += result["extracted"]
        tag_count += result["contains_tag"]

    run(["grep", "--recursive", "-i", "password"], cwd=folder)
    print(f"Total files extracted: {total_files}")
    print(f"{tag_count} contains tag {tag}")


if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Extract and recursively get EXIF data from a zip file")
    arg_parser.add_argument("archive", type=Path, help="The zip to work with")

    parser = arg_parser.parse_args()
    file_path = parser.archive

    if file_path.is_file() and is_zipfile(file_path):
        exiftool.executable = "/home/voldy/Tools/Image-ExifTool-11.94/exiftool"
        extract_examine(file_path)
    else:
        print(f"Error! {file_path} is not a valid file/zip")

