# cmd1

## Challenge Resources

Service: SSH <br/>
Server: `pwnable.kr` <br/>
Port: 2222 <br/>
Username: _coin1_ <br/>
Password: _guest_ <br/>

## Writeup

This challenge was less about over-coming `$PATH` vulnerability but more about figuring out to how to bypass the `filter()` function using _sh regex expansions_.
The program takes a shell command as command-line argument and passes it through the `filter()` function before executing it using `system()`.
The `filter()` function looks for the words _sh_, _tmp_ and _flag_ in the command-line arguments and if they're found it returns the non-zero number which cause `main()` to immediately return. If not then, the `system()` is called with the command-line argument.
Since the `filter()` uses standard library's `strstr()` and `system()` runs our input within a `sh` shell, we can easily fool the `filter()` by using shell globs. I used this shell glob to execute `cat` and read the file.

```bash
/bin/cat /home/cmd1/fl?g
```

N.B: Trying to spawn a shell to execute commands may not be a good idea, since starting with **Bash v2**, **bash drops privileges if group id doesn't matches the user id**
