SSH running @ port 22. OpenSSH 7.6p1
Werkzeug httpd server @ port 80. It's a python development server.

Tried "/etc/passwd". Didn't work. Writeup says "../../../../etc/passwd" works.
It does cause the webapp searches expands the prepends "/home/falcongfeast/articles".
And keeping that in mind "../../../../etc/passwd" will bypass that.

Exploited LFI. The passwd file has the user's falconfeast's credentials.
Used that to SSH into machine, got the user flag.
Used sudo -l to get my sudo config, saw that socat can be run without password.
Used techniques from https://gtfobins.github.io/gtfobins/socat/ to get a reverse shell using socat.
This dropped a root shell. Then cat root.txt
