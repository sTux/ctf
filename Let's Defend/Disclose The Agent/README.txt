See all SMTP packets with the respective TCP streams
- tcp.stream == 1 && smtp

All the details can be found there. 

Export the SMTP message to a file. Remove protocol text to keep the base64 encoded text of the attachment. Then,
- cat message | base64 -d | tee secretrendezvous.docx
