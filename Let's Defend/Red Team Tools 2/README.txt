Go to the host in Endpoint Secuirty. Looking at the command history we can see that an reverse shell was created at 21.12.2020 13:00.
It's safe to assume that the attack started during that time. We shall consider this timeframe while investigating this attack.

1 & 2: Open the endpoint network connection logs during that time and we can one such IP: 221.181.185.200. VirusTotal can reveal the origin
3: We can a certutil invocation to download a executable named services.exe. To understand what tool was used we can some of the strings in the fireeye tools yara 
4: The tool name and string suggests it uses WMI thus uses WQL to query the information
5: There's only one command to create a user. We can get the password from there.
6 & 7: Searching about the incident would open articles that refers to the CVEs and names of the tools used in the attack