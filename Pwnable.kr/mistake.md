# mistake


## Challenge Resources
Url: mistake@pwnable.kr <br/>
Port: 2222 <br/>
Password: guest <br/>


## Writeup
This challenge was on C operator precedence. A good refernce can be found [here](https://en.cppreference.com/w/c/language/operator_precedence).
Looking at line number 17 in the C souce code, we can see that a open call is made. The catch here is the program doesn't have permission the *password* file.
Since the assignment operator, a.k.a `=` operator has a lower priority than `<`, the value that is assigned to the variable `fd` is the value of `open(...) < 0` which eveluates to 1, since the target file can't be opened.
This causes the `read()` call at line 27 to read from stdin, rather than reading from the file. Also another problem I faced was if the inputs were less than 10 characters, the challenge fails `¯\_(ツ)_/¯`.
So using this python code, we can generate both the inputs required to complete the challenge `"".join([chr(ord(i) ^ 1) for i in "0123456789"])`
