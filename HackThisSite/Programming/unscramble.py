sorted_words = {}

with open("wordlist.txt", "r") as fin:
    for word in fin:
        word = word.strip()
        sorted_word = "".join(sorted(word))
        sorted_words[sorted_word] = word
        # print(f"{word=} {sorted_word=}")

while True:
    try:
        word = input()
        word = "".join(sorted(word))

        if word in sorted_words:
            print(sorted_words[word], end=",")
    except EOFError:
        break

print("")

