package com.lfalch.crackme1;

import java.awt.*;
import javax.swing.*;

public final class MainPanel extends JPanel
{
    JTextField user_name_textbox;
    JTextField user_password_textbox;
    
    public MainPanel() {
        super();
        this.setLayout(null);
        
        final JLabel headingText = new JLabel("LFalch 1st CRACKME");
        headingText.setBounds(10, 11, 228, 27);
        headingText.setFont(new Font("Thoma", Font.BOLD, 22));
        this.add(headingText);
        
        final JLabel nameLABEL = new JLabel("Name:");
        nameLABEL.setBounds(10, 79, 46, 14);
        this.add(nameLABEL);

        final JLabel serialLABEL = new JLabel("Serial:");
        serialLABEL.setBounds(10, 117, 46, 14);
        this.add(serialLABEL);

        (this.user_name_textbox = new JTextField()).setBounds(45, 76, 180, 20);
        this.add(this.user_name_textbox);
        this.user_name_textbox.setColumns(10);

        (this.user_password_textbox = new JTextField()).setColumns(10);
        this.user_password_textbox.setBounds(45, 114, 180, 20);
        this.add(this.user_password_textbox);

        final JButton verifyBTN = new JButton("Verify");
        verifyBTN.setFont(new Font("Thoma", Font.BOLD, 16));
        verifyBTN.addActionListener(new VerifyButtonListener(this));
        verifyBTN.setBounds(10, 145, 215, 50);
        this.add(verifyBTN);
    }
    
    static JTextField get_user_name_textbox(final MainPanel mainPanel) {
        return mainPanel.user_name_textbox;
    }
    
    static JTextField get_user_password_textbox(final MainPanel mainPanel) {
        return mainPanel.user_password_textbox;
    }
}
