﻿# Day 4 - Training
This one are basic Linux tasks. _No hacking skills required_

## How many files?
```bash
ls | wc -w
```

## file5 content...
```bash
cat file5
```

## Which file has the "password"?
This was a unknown challenge for me and I needed google. I did knew that it was possible via terminal, I was not sure what the command, but I eventually found out
```bash
grep -iRl "password" ./
``` 

## What's the IP?
Same as the previous one, but now with regex-fu
```bash
grep -iRl "[[:digit:]]\{2\}\.[[:digit:]]\.[[:digit:]]\.[[:digit:]]\{2\}" ./
```
finds which file has the IP address. Now to get the IP
```bash
grep -o "[[:digit:]]\{2\}\.[[:digit:]]\.[[:digit:]]\.[[:digit:]]\{2\}" ./file2
```


## How many users?
Pretty sure I did the wrong way, but I can be calculated as

 - *_root_* :- The superuser
 - *_ec2-user_* :- A default user for Amazon EC2 instances
 - *_mcsysadmin_* :- The user we logged in for the challenge
So 3 users

## file8 sha1 hash can be....
...found out via 
```bash
sha1sum file8
```

## And the hash can be...


> Written with [StackEdit](https://stackedit.io/).
