oledump.py reports 2 streams containing Macros and 1 stream containing VBA attributes. Thus we can find the answer to the first question

Using olevba3 we can see that the maldoc starts with Document_open

Searching for the maldoc hash in VirusTotal, we can find that this is a Emotet maldoc

There is atleast one more stream containing VBA form code and encoded strings. But stream 34 looks like it contains an obfuscated base64 string

Using oledump we can see all the macro steams and we can a stream called Macros/roubhaol/f, which mean this the form we need called "roubhaol"

Let's dump all the macros using olevba3, we can see the macro present in Macros/VBA/govwiahtoozfaid. From the function juuvzouchmiopxeox(), we can find the padding

Now we have the pad and the obfuscated Base64, let's de-obfuscate the string and discover the powershell command. Run decode_base64.py to get that.

Now we have the powershell command in Base64, decode it to find the URL and binaries it invokes
