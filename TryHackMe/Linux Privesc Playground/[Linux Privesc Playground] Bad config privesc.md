﻿# Linux Privesc Playground
This is my write-up of the **Linux Privesc Playground** room and how bad configuration of `sudo` can lead to privilege escalation.

## Examing the **/etc/sudoers** file
The challenge description explicitly states that this challenge is about bad superuser configurations and bad SUID bit. Following that description, my first instinct was to look into the heart of `sudo`'s configuration, the `/etc/sudoers` file.
A glance at the file and it starts with similar configs, until we get to the very end where we can see the bad configurations.
The user `user` is allowed to run multiple binaries without requiring any password. These binaries included bash, vim, python and other programs that can drop us into a shell. 

## First method
After I completed the examination of the `/etc/sudoers` file, the easiest solution seemed to be using
```sh
sudo bash
```
Dropped me right into a root shell. From there just went and grabbed the flag

## Second method
I really wanted to see if I can use **"incorrectly set SUID bits"** to escalate privileges. So first things first, enumerating the system using `find` utility. A huge list of binaries pop up. Not much of a use. Why not try the `find`-`whoami` technique? Did just that
```bash
find / -user root -perm -4000 -exec whoami \;
```
And unsurprisingly, the output is `root`. So let's just tweak the previous command to get the flag
```bash
find / -user root -perm -4000 -exec cat /root/flag.txt \;
```
Got the flag again!

> Challenge notes present here: [challenge notes.txt](challenge notes.txt)
> Written with [StackEdit](https://stackedit.io/).
