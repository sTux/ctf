# Day 17 - Hydra-ha-ha-haa
This challenge introduced the hydra login bruteforcing tool


## Finding the web login
Starting the task, it was asked to crack the web login password for the molly account. So I armed Hydra and fired it on the website to crack the password using
```bash
hydra -l molly -P ~/rockyou.txt 10.10.36.28 http-post-form "/login:username=^USER^&password=^PASS^:Your username or password is incorrect" -V
```
This will crack the password. Login into the web site using the found credentials and grab the flag. Remember to make the correction
*N.B: Don't belive the hint. The password does not lie b/w the first 30 passwords. According to [this writeup](https://www.embeddedhacker.com/2019/12/hacking-walkthrough-thm-cyber-of-advent/), it's in the 900K+ range. I copied the password after 10K attempts*


## Cracking the SSH
Now that we have logged into molly's web account, let's break into molly's SSH. Again we can fire hydra for it
```bash
hydra -l molly -P ~/rockyou.txt ssh://10.10.36.28 -V
```
This command crack the password within seconds and now we the SSH credentials for molly. Login into molly's SSH using the found credentials and grab the flag
