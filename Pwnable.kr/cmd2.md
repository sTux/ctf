# cmd2

## Challenge Resources

Service: SSH <br/>
Server: `pwnable.kr` <br/>
Port: 2222 <br/>
Username: _coin2_ <br/>
Password: `<flag of coin1 challenge>` <br/>

## Writeup

This challenge was about over-coming `$PATH` over-write and bypassing `filter` function to read the flag _sh regex expansions_.

The program takes a shell command as command-line argument and passes it through the `filter()` function before executing it using `system()`.
The `filter()` function looks for the words _export_, _PATH_ and _flag_, along with characters like _=_, _/_, _`_.
If they're found in the command-line arguments the function returns a non-zero number which cause `main()` to immediately return.
If not then, the `system()` is called with the command-line argument.
Since the `filter()` uses standard library's `strstr()` and `system()` runs our input within a `sh` shell, we can fool the `filter()` by using shell globs and either with shell builtins or encoded strings.

I first thought of using the octal ASCII values of _/_ to bypass the filter function. I finally used this to get the flag

```bash
./cmd2 '$(echo "\\57bin\\57cat fl?g")'
```

After unlocking the write-up, I saw one entry using the `command` built-in. I google about it and came to know that `command` can be used to execute commands and get the result of the command. When used with `-p`, then it uses the overrides the inherited _$PATH_ and uses the default _$PATH_ in order to ensure all binaries are avaliable during the `command` execution. This can be used, along with shell globs, to bypass the _$PATH_ override.

```bash
./cmd2 "command -p cat fl?g"
```
