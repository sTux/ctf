Load the PCAP in Brim and Wireshark

Q1: Statistics > Capture File Properties. Find the Packets to get the total number of packets.
Q2: To get the Timestamp of first packet View > Time Display Format > UTC Date and Time of the Day.
Q3: To get the duration of capture Statistics > Capture File Properties > Elapsed
Q4,Q5,Q6: Statistics > IPv4 Statistics > All Addresses to get most active address. Use ip.src_host == 10.4.10.132 to get MAC ID.
Q7: From Statistics > IPv4 Statistics > All Addresses, 4 IP in range 10.4.10.x. Thus can assume network is 10.4.10.0/24.
Q8: DHCP has an option to send hostname as part of the client requet. Use dhcp as Wireshark filter to get DHCP from 10.4.10.132
Q9: To get DNS server IP, used in Brim _path=="dns" query=="bot.whatismyipaddress.com" and Wireshark dns
Q10: Navigate to packet number 204, it's a DNS request
Q11: From the previous packet detail, we can get the DNS response
Q12: Now we've the IP, we can get the country using VirusTotal
Q13: We can get the OS by looking at the user-agent of the device. Open any http packet from 10.4.10.132
Q14: Use Brim to get all the HTTP and we can see an PE file being requested. Search that stream in Wireshark to export the file
Q15: Brim gives the MD5 of the file
Q16: Search the sample in VirusTotal to get the MalwareBytes name
Q17: From the HTTP logs of the PE file, we can get the server software from HTTP traffic
Q18: Brim show another connection to HTTP service for getting public IP. Search for these logs in Wireshark and follow HTTP stream
Q19: Since Brim shows that SMTP traffic is present, we can search for logs in Wireshark. Headers give away the IP and from there country
Q20: From the SMTP logs, we can see the mail and domain to which the exfiltrated data is sent. Search that domain in VirusTotal
Q21: From the same logs, we can get the software running on the server
Q22, Q23, Q24, Q25: Open any of the E-Mail network logs, to get a full log of the conversation of each mail. Copy and decode as needed
Q26: Viewing SMTP logs in Brim, allows us to get the time quantum pretty easily


Follow SMTP traffic and we can see login data being exfiltrated