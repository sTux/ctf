Event ID: 56
Rule Name: SOC123 - Enumeration Tool Detected
Source Address: 172.16.20.4
Destination Address: 185.199.109.133
Alert Host: gitServer
Trigger URL: https://raw.githubusercontent.com/rebootuser/LinEnum/master/LinEnum.sh


Looking at the triggering URL, we know that a correct rule was triggered. Now we need to know if the tool was indeed run on the system.
The event was triggered on Fed. 13 2021, 4:47 P.M and if we see that the command history then we can that the file was run at 4:50 P.M
