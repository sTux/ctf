import requests
from sys import exit


def findFlag(path="", flag=""):
    response = requests.get(f"http://10.10.241.214:3000/{path}")
    response = response.json()
    if response["value"] == "end":
        print(f"The flag is {flag}")
        exit(0)
    flag += response["value"]
    print(flag)
    next = response["next"]
    findFlag(path=next, flag=flag)


if __name__ == "__main__":
    findFlag()

