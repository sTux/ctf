package agz.name.c005;

import I.I;
import java.util.Base64;
import java.util.Scanner;

/* compiled from: agz/name/c005/Crackme005 */
public class Crackme005 {
    private static boolean is_double(String str) {
        /**
         * Checks if the passed string is a valid double or not
        **/
        try {
            Double.parseDouble(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static double availableProcessors(String str) {
        // Couldn't figure out
        StringBuilder stringBuilder = new StringBuilder();
        for (char append : str.toCharArray()) {
            stringBuilder.append(append);
        }
        return new Double(stringBuilder.toString()).doubleValue();
    }

    private static double decode(double d) {
        // A basic hash function
        if (d == 0.0d) {
            System.out.println("Sorry, you need to enter an code");
            System.exit(0);
            return 0.0d;
        }
        /**
         * As of 30/12/2019, the values I get on my machine is:
         * Runtime.getRuntime().totalMemory() = 132120576
         * Runtime.getRuntime().availableProcessors() = 8
        **/
        double totalMemory = (double) 132120576;
        return (
            (
                (
                    (double) 224
                ) + 
                (d - 1.32120576)
            ) + 
            (totalMemory / 107543.0d)
        ) - 
        (
            totalMemory / (
                            (double) Double.toString(d).length()
                          )
        );
    }

    private static String double_toString(double d) {
        // Returns a string of the parameter
        return Double.toString(d);
    }

    private static String getInput() {
        // Wrapper method to return input from stdin
        return new Scanner(System.in).nextLine();
    }

    private static void printMessage(int i) {
        // The starting greetings
        System.out.println("");
        System.out.println("AGZ Crackme no " + i);
        System.out.println("There will be some fun in here");
        System.out.println("See if you can beat the encryption");
        System.out.println("");
    }

    private static boolean getDecoder(double d, double d2) {
        // Checks if both the double are equal, using String's equal
        return double_toString(d).equals(double_toString(d2));
    }

    private static String decode_base64(String str) {
        // Base64 decodes the parameter string
        return new String(Base64.getDecoder().decode(str));
    }

    private static void print_final_message() {
        // The ending message
        System.out.println("");
        System.out.println("Got the solution?");
        System.out.println("ship it off to me then..");
        System.out.println("crackme@agz.name");
        System.out.println("https://www.agz.name");
        System.out.println("https://www.facebook.com/AGZ.name/");
    }

    private static void length(boolean z) {
        // Prints either the success or failue mesage
        if (z) {
            System.out.println("Congratz, now write me and keygen with a explanation");
        } else {
            System.out.println("Sorry laddie, need some more debugging here...");
        }
    }

    public static final void main(String[] args) {
        printMessage(5);
        System.out.println("Username");
        String username = getInput();
        System.out.println("Enter a number");
        String code_input = getInput();
        if (is_double(code_input)) {
            double code_double = Double.parseDouble(code_input);
            double decode = decode(code_double);
            double decode2 = decode(availableProcessors(username));
            int i = (int) decode2;
            int i2 = (int) decode;
            length(getDecoder(decode2, code_double));
        } else {
            System.out.println("Sorry, only Double accepted.");
        }
        print_final_message();
    }
}
