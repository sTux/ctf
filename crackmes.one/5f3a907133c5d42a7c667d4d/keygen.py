from random import choice
from random import randint

allowed_characters = ['a', 'b', 'd', 'e', 'f', 'i', 'm', 'o', 'p', 'q', 's', 'x', 'w', 'z']
length = randint(8, 20)		# The password needs to atleast 8 characters long
string = ""

for _ in range(length):
	string += choice(allowed_characters)


print(f"Use string {string} for both username and password")
