import struct
from sys import byteorder


first_round = ""
with open("dat_secret.encode", "rb") as secret:
    # Decode the file as 31 bytes tuple
    secret = struct.unpack('31c', secret.read())
    # print(secret)

for char in secret:
    char = int.from_bytes(char, byteorder)
    char = (((char >> 4) | ((char << 4) & 240)) ^ 41)
    first_round += chr(char)

print(first_round)