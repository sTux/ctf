In this problem, we are given a simple `assembly` program, which has only one function `asm0`.
The function takes 2 arguments, and copies both the arguments first to **eax** and **ebx** registers respectively.
Once done, it then simply copies the **ebx** registers to **eax** registers and returns.

So the answer is the 2nd argument
