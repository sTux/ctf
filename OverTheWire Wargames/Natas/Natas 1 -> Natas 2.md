# Natas 1 -> Natas 2


## Target
Url: [http://natas1.natas.labs.overthewire.org/](http://natas1.natas.labs.overthewire.org/) <br>
Username: natas1 <br>
Password: gtVrDuiDfck831PqWsLEZy5gyDz1clto <br>


## Next Password
This level introduced us that some websites block right-clicking as a Pseudo-Security measure XD. The password can easily be found by using _CTRL+U_ to open the source code of the page.
Though if we right-click outside the white zone on the page, we can see that we can actually get the menu to open source :P
