# Bandit Level 30 → Level 31


## Question
There is a git repository at **ssh://bandit30-git@localhost/home/bandit30-git/repo**. The password for the user **bandit30-git** is the same as for the user **bandit30**.

Clone the repository and find the password for the next level.


## Solution
This level is to do with tags. Just get all the tags from the remote and use `git tag` to list all the tag and `git show <tag-name>` for the tag info

P.S: The Level 31 password is 47e603bb428404d265f59c42920d81e5



<small> This level was a little bit frustrating as the password looked like a git reference </small>
