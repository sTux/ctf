package p000I;

import java.io.InputStream;

/* compiled from: I/I */
/* renamed from: I.I */
public class I {
    static byte[] getResourceAsStream;
    static String[] intern = new String[256];
    static int[] read = new int[256];

    static {
        try {
            InputStream data_file = new I().getClass().getResourceAsStream("I.gif");
            if (data_file != null) {
                int read = data_file.read() | ((data_file.read() << 16) | (data_file.read() << 8));
                getResourceAsStream = new byte[read];
                int i = 0;
                byte b = (byte) read;
                byte[] bArr = getResourceAsStream;
                while (read != 0) {
                    int read2 = data_file.read(bArr, i, read);
                    if (read2 == -1) {
                        break;
                    }
                    read -= read2;
                    read2 += i;
                    while (i < read2) {
                        bArr[i] = (byte) (bArr[i] ^ b);
                        i++;
                    }
                }
                data_file.close();
            }
        } catch (Exception e) {
        }
    }

    /* renamed from: I */
    public static final synchronized String I(int i) {
        String str;
        synchronized (C0000I.class) {
            int i2 = i & 255;
            if (read[i2] != i) {
                read[i2] = i;
                if (i < 0) {
                    i &= 65535;
                }
                intern[i2] = new String(getResourceAsStream, i, getResourceAsStream[i - 1] & 255).intern();
            }
            str = intern[i2];
        }
        return str;
    }
}
