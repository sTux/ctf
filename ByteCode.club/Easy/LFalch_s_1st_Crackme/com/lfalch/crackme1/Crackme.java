package com.lfalch.crackme1;

import javax.swing.*;
import java.awt.*;

public class Crackme extends JFrame
{
    public Crackme() {
        super();
    }
    
    public static void main(final String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            System.out.println("Couldn't get that look and feel");
        }
        final Crackme crackme = new Crackme();
        crackme.setTitle("LFalch's 1st crackme");
        InputVerification.set_constants("LFalch", "password123");
        crackme.setPreferredSize(new Dimension(250, 230));
        crackme.setResizable(false);
        crackme.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        crackme.getContentPane().add(new MainPanel());
        crackme.pack();
        crackme.setVisible(true);
    }
}
