﻿# Day 9 - Requests
This challenge is not a hacking challenge but is just a introduction to **Python**'s **requests** package and _JSON_ format. It just needs a small script and what the script should do is made extremely made clear by the challenge statement. Here is my [script](day-9.py) and here's the output
![script output](script_output.png)


> Written with [StackEdit](https://stackedit.io/).
