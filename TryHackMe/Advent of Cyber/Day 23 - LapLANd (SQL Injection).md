# Day 23 - LapLANd
This challenge introduced **SQL Injection** and how to _exploit it to gain access_


Before I started with any of the tasks, I did a quick _NMAP Version Scan_ to get the list of services running on the server.
    - Port 80 for Apache httpd 2.4.29 Server
    - Port 22 for OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 Server

## Which field is SQL injectable? Use the input name used in the HTML code.
Knowing that a HTTP Server is running, I used `sqlmap` to look for SQL Injection attacks in the web application. It reported the vulnerability as
```
Parameter: log_email (POST)
    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: log_email=fxlX' AND (SELECT 8287 FROM (SELECT(SLEEP(5)))cdaZ) AND 'bCZD'='bCZD&log_password=&login_button=Login

    Type: UNION query
    Title: Generic UNION query (NULL) - 12 columns
    Payload: log_email=fxlX' UNION ALL SELECT NULL,NULL,NULL,CONCAT(0x7171626a71,0x49665967596d4e52624344654d4659584f4258546f524c6857674f54796b6e616d615a7870497072,0x7171627071),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL-- -&log_password=&login_button=Login
```
So the web application suffers from UNION query Injections and Delay based BLIND Injections. `sqlmap` also reported following about the DB
```
back-end DBMS operating system: Linux Ubuntu
back-end DBMS: MySQL >= 5.0.12
banner: '5.7.28-0ubuntu0.18.04.4'
current user: 'santa@localhost'
current database: 'social'
current user is DBA: True
```

## What is Santa Claus' email address?
Re-ran `sqlmap` to now dump the tables of database _social_. Found the table _users_. Now on this re-run, dumped the entire table _users_. From that dump, I found the data for santa.

## What is Santa Claus' plaintext password?
From the dump, got the hashed password. Searched it in _crackstation.net_ and found the password.

## Santa has a secret! Which station is he meeting Mrs Mistletoe in?
This can be found out by just looking around.

## Once you're logged in to LapLANd, there's a way you can gain a shell on the machine! Find a way to do so and read the file in /home/user/
Now that I've logged-in to the system, I tried finding how to get into the server. Looked at the hint and it hinted at reverse shell. And that quickly drew the attention to the "Upload File" button. I tried uploading _.php_ but that was blocked. I then tried _.phtml_ and it was accepted. With that, I created a new post. Now using _DevTool_ inspected the new post and found the link of the uploaded reverse shell as `<img>`. Visited the link and got a reverse shell back. Now navigated to **/home/user/** to fetch the flag
