Event ID: 81
Trigger Rule: SOC134 - Suspicious WMI Activity
Alert Host: Exchange Server
Alert Host IP: 172.16.20.3
Sample Hash: f2b7074e1543720a9a98fda660e02688
Event Time: March 15, 2021, 10:57 p.m.
Action: Cleaned



Download and search the hash in VT.
VT: https://www.virustotal.com/gui/file/4ea1f2ecf7eb12896f2cbf8683dae8546d2b8dc43cf7710d68ce99e127c0a966/detection

From the comments we can get a clue that this malware was part of a YT video and that we can find the source of the malware on GH
GH repo: https://github.com/FlyTechVideos/000exe

Inspecting the source, we can multiple WMIC commands about renaming the current user account and turing on AutoLogon in the account.
Plus from the YT video, we understand that this was intended to malware. So treating this malicious TP.