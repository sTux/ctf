﻿# Day 12 - Elfcryption
This was an introduction to the cryptography using **gpg** and **openssl** tools.
There was not much to too. Also all the passphrase are given as hints.

The _tosend.zip_ file contains
- `note1.txt.gpg`:  AES encrypted file
- `note2_encrypted.txt`: RSA encrypted file
- `private.key`: A RSA private key
> If you have been done these challenges for few time now, you may be tempted to import the `private.key` into **gpg**, then know that it's not a PGP key file.

To get the _MD5 checksum_ of `note1.txt.gpg`, simply run the **m5sum** utility on
```bash
md5sum ./note1.txt.gpg
```

From the extension, you can understand it's encrypted using **gpg**. To decrypt the `note1.txt.gpg` , simply use
```bash
gpg -d ./note1.txt.gpg
```

Now for `note2_encrypted.txt`, we can deduce that the file has not been encrypted using **gpg**,  so let's try **openssl**. Also since we have a unsed **private.key** key-file, we can deduce that this file is enctypted using _RSA_ algorithm
```bash
opnssl rsautl -decrypt -inkey ./private.key -in ./notes2_encrypted.txt -out decrypted.txt
```
This will decrypt the _RSA_ encrypted file using the _RSA private key_

> Written with [StackEdit](https://stackedit.io/).
