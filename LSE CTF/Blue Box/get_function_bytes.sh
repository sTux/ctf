echo -n "The bytes for function: "
objdump -D re30 --start-address=0x08048550 --stop-address=0x08048591 | tail -n 20 | cut -f 2 | sed -E "s/(00|ff)//g" | sed 's/[[:alnum:]]\{1,\}/\\x\0/g' | tr -d [:space:]
echo ""
