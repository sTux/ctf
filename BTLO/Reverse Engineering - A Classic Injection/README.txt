Open the sample in different CFF Explorer. PE-Studio and DiE gives wrong version detections

Open the file in Ghidra and locate the WinMain file. See the Sleep function call as one of the first calls. Convert the parameter to minutes

Just after the sleep call is executed, we can see a unmatched function using the C++ std:cin, thus we can assume this takes the password.
Then we can see like a C string is being loaded in a variable and then being used what looks a array comparison with a constant "btlo"

We can the sample copies 118 bytes from rodata to a buffer and that is immediatedly followed by creating a suspended process.
The sample then calls WriteProcessMemory, the 4th parameter is the size of shellcode

After the memory is written, the suspended and overwritten process is created.

Execute the sample with filters on the sample name, nslookup.exe and powershell.exe, with focus on File and Process operations.

We now know nslookup.exe is the victim process (can verify in trace), we can follow the trace, can see that nslookup.exe creates a process.
The process created is a powershell process with a Base64 encoded command string.

Copy the command string and decode it. We can find the file created, contents written too.