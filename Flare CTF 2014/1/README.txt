Sample is a .NET binary with a Form in it.

In the disassembly we can see the Decode button performs 3 rounds of "decoding" on the file contents. But when we run the app, the result is garbage.
Thus we can assume that either the rounds obfuscate furthur or it deobfuscates and re-obfuscates the string.
To test the latter argument, we try to use only the first round on the bytes. Doing so reveals the flags
