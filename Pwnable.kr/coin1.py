from pwn import *
from re import findall


pwnable = remote("pwnable.kr", 9007)
pwnable.recvuntil("\t- Ready? starting in 3 sec... -\n\t\n")
for _ in range(100):
    start = 0
    end, chances = map(int, findall(r"\d+", pwnable.recvline().decode()))
    context.log_level = "debug"
    end -= 1

    mid = (start + end) // 2
    while chances > 0:
        pwnable.sendline(" ".join(map(str, range(start, mid))))
        weight = int(pwnable.recvline())
        if weight%10 != 0:
            end = mid
        else:
            start = mid
        chances -= 1
        mid = (start + end) // 2
    pwnable.sendline(str(mid))
    print(pwnable.recvline())
pwnable.recvline()
print(pwnable.recvline())
