import requests


url = "http://10.10.254.210:8000/api/{}"

for i in range(1, 101, 2):
    curl = url.format(i)
    response = requests.get(curl)
    response = response.json()

    if response['q'] != "Error. Key not valid!":
        print(f"{curl} => {response['q']}")

