def decode(encoded_password):
    key = 0x7b1
    password = []

    for c in encoded_password:
        key = key * 7 & 0xffff
        password.append(ord(c) + ((key // 10) * ord('\n') - key))

    print("".join(map(chr, password)))


if __name__ == '__main__':
    strings = ["fhz4yhx|~g=5", "Ftyynjy*", "Zwvup(", "Gtu.}\'uj{fq!p{$", "Lszl{{%\x82vx{!whvt|twg?%"]
    
    for s in strings:
        decode(s)

