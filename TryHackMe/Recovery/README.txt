Use scp to copy the file to the analyst machine. Open the binary in IDA, get the decompiled code.

First we can see the binary appends a infinite loop at the end of bashrc file. We also see the logging.so library being backed up and overwritten.
The binary then execute /bin/admin with the input of "pwned"

Looking into using /bin/admin using basic analysis reveals that liblogging.so, we can assume that the invocation of the binary starts the infection

So we copy that library from the machine for analysis. Disassmble the library using IDA and generate the pseudocode using the online disassembler.


To repair the webserver, we would need root access. Replace the script in /opt/brilliant_script.sh that would replace /root/.ssh/authorized_keys with out SSH keys.