The length of the password is **7**.
The target function we need to analyze is the `checkPassword` function.

The function takes in the input password.
First thing it does it **creates a string containg the letters 'dec'**.
Then it creates a **accumulator string**, and the characters **'de'** us copied from the **string containg 'dec'**.
Then the characters **'k'** and **'car'** is appended to the **accumulator string**. After that the **'c'** is copied into the **accumulator string**.
This thus creates the target password to be **dekcarc**, which is the reverse of **cracked**.


Thus the required password we need give is **cracked**.
