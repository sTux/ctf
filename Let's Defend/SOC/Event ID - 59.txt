Event ID: 52
Rule Name: SOC101 - Phishing Mail Detected
SMTP Address: 27.128.173.81
Source Address hahaha@ihackedyourcomputer.com
Destination Address: mark@letsdefend.io
Action: Blocked


Since this SMTP server address is on the internet, first we check the reputation of the address using Talos intelligence
Talos: https://talosintelligence.com/reputation_center/lookup?search=27.128.173.81 (Poor)
Network log shows that the server indeed contacted to send the email to mark@letsdefend.io, but the email was blocked from reaching the mailbox.
Also the content of the email is very suspicious and looks very phisy.