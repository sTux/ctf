# random


## Challenge Resources
Url: ssh -P 2222 random@pwnable.kr
Password: guest


## Writeup
This challenge was about the problems that can happen if we try to use a unseeded PRNG in our program
Using a static or non-random seed for PRNG makes the generator totally predictable since the initial state for most generators are extremely well-known and documented, for libc's rand() it's 1.
Since the generator was not seeded, The calls to the rand() function always genrates predictable and same values. To verify we can use GDB as:
```gdb
break *0x400606
run
print $eax
```
The value always remains the same across runs. Once we get this value, we can simply xor it with 0xdeadbeef to get the target input

```bash
python3 -c "print(0x6b8b4567 ^ 0xdeadbeef)" | ./random
```
