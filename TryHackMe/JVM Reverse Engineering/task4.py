encoded_password = "aRa2lPT6A6gIqm4RE"
password_length  = len(encoded_password)
decoded_password = ""


for i in range(password_length):
    decoded_password += chr(ord(encoded_password[i]) ^ i % 3)

print(decoded_password)
