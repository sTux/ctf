Event ID: 50
Trigger Rule: SOC117 - Suspicious .reg File
Source Address: 172.16.17.51
Sample Hash: f705ac114767397fb5e7cd1603e70954


Download the sample zip. We have 2 files, the snapshot of the Windows registry and a suspicious looking batch file.
Hash the file and search in VirusTotal, and we can get a confirmation that it's malicious.
VirusTotal: https://www.virustotal.com/gui/file/fc4de26ede0690dbc4ef4ed7ffcc28c086d5c8998f2cbe1e2c3c20516c7da2db

Both from the .bat file source and VirusTotal, we can confirm that this malicious file has worming capabilities.
From the Relationships in VirusTotal, we can see that this malicious batch file has a malicious parent
VirusTotal: https://www.virustotal.com/gui/file/4c64a5349b5b9dda48a01a1cf55a760c04815867b32b4088dc3a3a1eade08de8

Even this malicious file was used, we know that it was cleaned and also that this warm expects a 192.168.1.x IP space, which is not used here.
