---
title: HackTheBox - Writeup
created: '2021-07-03T16:59:18.459Z'
modified: '2021-07-05T22:32:56.491Z'
---

# HackTheBox - Writeup

## Scanning the box
- Used `rustscan 10.10.10.138` to quickly scan all the ports in the machine.
  - Port 80
  - Port 22

- Used `nmap -A -v -p22,80 10.10.10.138` to script scan, version scan for the open ports
  - 22/tcp open  ssh     OpenSSH 7.4p1 Debian 10+deb9u6 (protocol 2.0)
  - 80/tcp open  http    Apache httpd 2.4.25 ((Debian))

---------------------------------------------------------------------------------------------------------------------------------
### Fuzzing and bruteforcing tools won't work because of DoS protections

- No `nikto -h 10.10.10.138` to scan the HTTP server
- No `gobuster dir` to scan for hidden directories and files
---------------------------------------------------------------------------------------------------------------------------------

- Found `/robots.txt`
  - Disallowed entry - **/writeup**
    - Visited it, checked source, saw **CMS Made Simple**
      - Looks like vulnerable to **CVE 2019-9053**
        - Exploit code [here](https://gist.githubusercontent.com/pdelteil/6ebac2290a6fb33eea1af194485a22b1/raw/9a9032373cd4b7ba463d40f18cacebb0186bf366/gistfile1.txt) (Python 2)
        - Explanation [here](https://titanwolf.org/Network/Articles/Article?AID=7954cb9c-0998-4764-8597-711a36af30c5#gsc.tab=0)

## Exploiting the vuln
- Used the writeup after porting it Python 3. Found:
  - Salt - **5a599ef579066807**
  - Username - **jkr**
  - Email - **jkr@writeup.htb**
  - Hashed Password - **62def4866937f08cc13bab43bb14e6f7$**
  - Password - **raykayjay9**
  - Credential accepted for SSH. Got user foothold

## Priviledge escalation
- Started scanning using `LinPEAS`. Found:
  - CVE 2019-13272 vulnerability for **pkexec**
  - **/usr/local/bin** and **/usr/local/sbin** was writable

Got a bit frustated and took to the forums. Got nudged to use **pspy** and was told that having a lot of users on the box can help me
- Keeping that in mind started **pspy64**
  - First got down a rabbit hole with a root process
  - Then after 2nd nudge, tried to observe what happens when I login
    - Saw a dynamic MOTD script.
      - Read the script. Went for weak path vulnerabilty
        - Got root

