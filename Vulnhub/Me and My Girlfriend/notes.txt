Imported and booted the VM. Need to first discover it's IP. Fired up `netdiscover`. Found it located @ 192.168.56.9.
Started `rustscan 192.168.56.9` to find all open ports and then started a nmap aggressive scan `nmap -v -A 192.168.56.9`. Here's th result
rustscan
    Open 192.168.56.9:22
    Open 192.168.56.9:80
nmap
    PORT   STATE SERVICE VERSION
    22/tcp open  ssh     OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.13 (Ubuntu Linux; protocol 2.0)
    | ssh-hostkey: 
    |   1024 57:e1:56:58:46:04:33:56:3d:c3:4b:a7:93:ee:23:16 (DSA)
    |   2048 3b:26:4d:e4:a0:3b:f8:75:d9:6e:15:55:82:8c:71:97 (RSA)
    |   256 8f:48:97:9b:55:11:5b:f1:6c:1d:b3:4a:bc:36:bd:b0 (ECDSA)
    |_  256 d0:c3:02:a1:c4:c2:a8:ac:3b:84:ae:8f:e5:79:66:76 (ED25519)
    80/tcp open  http    Apache httpd 2.4.7 ((Ubuntu))
    | http-methods: 
    |_  Supported Methods: GET HEAD POST OPTIONS
    |_http-server-header: Apache/2.4.7 (Ubuntu)
    |_http-title: Site doesn't have a title (text/html).

So a Apache webserver with PHP is running. Visited a site. It displayed a message saying site can be accessed through localhost only.
This means that I need to spoof the client ip in the HTTP using X-Forwarded-For. This was also sugegested in the message's HTML comment that I noticed later

So I started using curl to spoof my IP to see if it works. Using `curl 192.168.56.9 -H "X-Forwarded-For: 127.0.0.1"` but everytime the empty response was returned. This threw me to the point I used a bruteforcing bash script to check which localhost IP they're whitelisting.
But turns out curl doesn't follow redirects by default, verifed by using `curl 192.168.56.9 -H "X-Forwarded-For: 127.0.0.1" --verbose`. -L flag needed to follow.
That confirmed the theory of client address spoofing. So used a Chrome extension to add this header by default. Now have access to full site.
Found the login page, tried to test SQL Injection but saw that I wasn't avaliable.
Looking at the index url **192.168.56.9/index.php?page=index**, thought there can be LFI but not vulnerable.
Without SQLi and LFI, next turn was **Broken Authentication**. So first registered and then logged into new account.
Navigated to the profile page, saw that the credentials are avaliable in the HTML and also that user-id exposed in URL like **http://192.168.56.9/index.php?page=profile&user_id=12**
Wrote a credentials grabber python script to find all the credentials. The script needs the VM ip and PHPSESSID cookie of a authenticated session.
Now since there's a credential for alice tried using it to login to SSH `ssh alice@192.168.56.9`. Logged In!
Found the flag in the home and a notes.txt. Read both of them. Not much usefull. Tried to enumerate for files owned by alice `find / -user alice 2> /dev/null`. The for SUID `find / -perm -4000 -user root 2> /dev/null`. Nothing useful.
Tried to see sudo permission using `sudo -l` and voila saw that I can run `(root) NOPASSWD: /usr/bin/php`.
So used `sudo php -a` to start a interactive PHP shell and then `system('id');` to confirm weather I'm root.
Yes I am! So used `system('bash');` to spawn a root shell, navigate to **/root** and grab the flag
