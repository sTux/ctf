import java.util.Random;


class Main {
	public static void main(String[] args) {
		char[] allowed_chars = new char[] {
			'0', '1', '2', '3', '4', '5', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
			'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
		};
		int length = allowed_chars.length;

		String name   = "oohhoohhoohhoohh";
		String author = "LFalch";
		Random random = new Random(name.hashCode() | author.hashCode());

		char[] serial = new char[16];
		for (int i=0; i<16; i++) {
			serial[i] = allowed_chars[random.nextInt(length)];
		}
		System.out.println(name + " : " + new String(serial));
	}
}
