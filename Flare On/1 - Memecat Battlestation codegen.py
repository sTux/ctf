def keygen():
    key = ord('A')
    target = ["\u0003", " ", "&", "$", "-", "\u001e", "\u0002", " ", "/", "/", ".", "/"]

    return "".join([chr(key ^ ord(c)) for c in target])


if __name__ == '__main__':
    print("Stage 1 code: RAINBOW", f"Stage 2 code: {keygen()}", sep="\n")