MOV        EDI, dword ptr [EBP + read_buffer]
MOV        ESI, dword ptr [EBP + password]
XOR        index, index

CMP        index, 0x8
JGE        LABEL_RETURN
MOV        BL, byte ptr [index + ESI*0x1]
MOV        AL, byte ptr [index + EDI*0x1]
; To get the cleartext, first the encrypted byte is XOR'd with the password byte
XOR        AL, BL
; This get a byte which then needs to left rotated by the current index value 
; and the current index value needs to be added to get the cleartext
ROL        AL, index
SUB        AL, index
MOV        byte ptr [index + EDI*0x1], AL
INC        index

