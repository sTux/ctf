import java.io.InputStream;

/**
Generic string decryptor and extractor used in these challenges
**/

class I {
   static byte[] IYOE;
   static String[] getClass = new String[256];
   static int[] getResourceAsStream = new int[256];

   public static final synchronized String I(int var0) {
      int var1 = var0 & 255;
      if (getResourceAsStream[var1] != var0) {
         getResourceAsStream[var1] = var0;
         if (var0 < 0) {
            var0 &= 65535;
         }

         String var2 = (new String(IYOE, var0, IYOE[var0 - 1] & 255)).intern();
         getClass[var1] = var2;
      }

      return getClass[var1];
   }

   static {
      try {
         InputStream var0 = (new I()).getClass().getResourceAsStream("" + 'I' + '.' + 'g' + 'i' + 'f');
         if (var0 != null) {
            int var1 = var0.read() << 16 | var0.read() << 8 | var0.read();
	    System.out.printf("File length: %d\n", var1);
            IYOE = new byte[var1];
            int var2 = 0;
            byte var3 = (byte)var1;
	    System.out.printf("Key for XOR %d\n", var3);
            byte[] var4 = IYOE;

            while(var1 != 0) {
               int var5 = var0.read(var4, var2, var1);
               if (var5 == -1) {
                  break;
               }
	       System.out.printf("Bytes read %d\n", var5);

               var1 -= var5;

               for(var5 += var2; var2 < var5; ++var2) {
                  var4[var2] ^= var3;
               }
            }

            var0.close();
         }
      } catch (Exception var6) {
         ;
      }

   }

   public static void main(String args[]) {
      // Change the values in the array for every challenege
	   int values[] = {1, 6, 12, 47, 51, 60, 76, 103, 109, 114};
	   for (int i: values) {
	   	System.out.printf("I.I(%d) = %s\n", i, I.I(i));
	   }
   }
}

