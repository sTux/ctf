from pwnlib.tubes.remote import remote
from re import findall
from itertools import accumulate
URL = "d913aa47d6bfc730.247ctf.com"
PORT = 50031

soc = remote(host=URL, port=PORT)
soc.newline = b"\r\n"
print(soc.recvline().decode())
print(soc.recvline().decode())

for i in range(500):
    line = soc.recvline().decode()
    numbers = sum(map(int, findall(r"\d+", line)))
    print(f"Query #{i+1}", line.strip(), numbers)
    soc.sendline("{}".format(numbers))
    print(soc.recvline().decode())

print(soc.recvline().decode())
soc.shutdown("read")
soc.shutdown("write")
