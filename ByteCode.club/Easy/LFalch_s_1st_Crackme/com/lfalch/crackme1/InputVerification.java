package com.lfalch.crackme1;

import java.util.*;

public final class InputVerification
{
    private static final char[] allowed_chars = new char[] {
            '0', '1', '2', '3', '4', '5', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
            'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    };
    private static String creator_name;
    private static String useless_string;

    public static boolean verify(final String user_name, String user_password) {
        user_password = user_password.toLowerCase();
        if (user_password.length() != 16) {
            return false;
        }
        
        final Random random = new Random(user_name.hashCode() | InputVerification.creator_name.hashCode());
        String string = "";
        for (int i = 0; i < 16; ++i) {
            string = string + InputVerification.allowed_chars[random.nextInt(InputVerification.allowed_chars.length)];
        }
        
        final byte[] bytes = InputVerification.useless_string.getBytes();
        for (int j = 0; j < bytes.length; ++j) {
            bytes[j] ^= 0x10;
        }
        InputVerification.useless_string = new String(bytes);
        
        return string.equals(user_password);
    }
    
    public static void set_constants(final String arg1, final String arg2) {
        creator_name = arg1;
        useless_string = arg2;
    }
}
