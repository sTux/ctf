﻿# Day 1 - Inventory Management

Upon visiting the site, we are faced with a simple login and registration page. Although we know the target account's username, we don't have the password. Also looking at the challenge statement, it became clear we needed to manipulate the authentication cookie to gain access to the account

## Task 1 - Finding the authentication cookies
So I created a dummy account and used it to login into the system. After the successful login, I inspected the response headers, to discover the cookies, that we set on response. The cookie that seemed interesting was `authid`. The value that was set on this cookie was a base64 code, which when decoded has our username. This seems to be our authentication cookie. To validate this, another dummy account was created and was logged-in. The authentication cookie sent from that login too had the cookie `authid` and the decoded value has our second dummy account's user-id. 


## Task 2 - Finding the static part of token
Now that we have our authentication cookie and we also have two cookies for the two accounts we control. The first thing I did was to decode both the cookies and compare both of them. This inspection reveals that the authentication cookie is made of _user-id +  "v4er9ll1!ss"_. Thus this is our static part of the authentication token.

## Task 3A - Gaining access to `mcinventory` account
This is the part that took me much time and honestly almost derailed me. Coming upto this part of the challenge we have the authentication cookie `authid` and the static portion of the authentication token. Since the token is basically Base64 of  _user-id +  "v4er9ll1!ss"_. And since we want to gain access to `mcinventory` account, we can just use the Base64 representation of _mcinventoryv4er9ll1!ss_, using the command 
```bash
echo "mcinventoryv4er9ll1!ss" | base64
```
or
```bash
printf "mcinventoryv4er9ll1!ss" | base64
```

But doing so in bash will give a error **_bash: !ss: event not found_** or for zsh, the error will be _**zsh: event not found: ss**_. This is because the `!` in these shell is reserved for a history search. Now on my shell I have not used any command that started with `ss` so such a event or input was not found and a error was given. The command that should be used is
```bash
echo 'mcinventoryv4er9ll1!ss' | base64
```
or
```bash
printf 'mcinventoryv4er9ll1!ss' | base64
```
The `''` is used instead of `""` because single-quoted in these shell means a string no string interpolation is not needed.

Once we have the base64 value of via the above commands, replace the `authid` cookie value in the browser with the calculated value. Refresh the page and voila, **_ACCESS GRANTED_**
## Task 3B - Exploring the admin
Now that we have access to the admin `mcinventory` account, the only task that remains is to look around and gather information. We were asked to find out, what the user ordered. Looking around the **Christmas Inventory Approval List**, we can find what the user ordered.
