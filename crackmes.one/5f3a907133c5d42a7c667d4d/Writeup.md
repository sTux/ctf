# Write-up

The application asks for a username and password and validates it using `Checker.IsValid()` method.
The method `IsValid()` method verifies the username and password with 3 distinct check blocks.
The first 2 blocks validates that the username and password is made up of only checks that checks to see if any character in the username and password is not amongst the whitelisted ones.

```C#
char[] allowed_characters = new char[] { 'a', 'b', 'd', 'e', 'f', 'i', 'm', 'o', 'p', 'q', 's', 'x', 'w', 'z' };
```

The first check is for the username, if the check fail a new window pops up with a message and the program exits

```C#
/** Username check begins here **/
this.username = this.username.ToLower();
int username_index = 0;
while (username_index < this.username.Length && is_valid) {
    if (this.username[username_index] < '0' || this.username[username_index] > '9') {
        bool is_allowed = false;
        int num2 = 0;
        while (num2 < allowed_characters.Length && !is_allowed) {
            if (this.username[username_index] == allowed_characters[num2]) {
                is_allowed = true;
            }
            num2++;
        }
        if (!is_allowed) {
            is_valid = false;
        }
    }
    username_index++;
}
if (!is_valid) {
    new Form2("Username contains wrong characters.").Show();
    return false;
}
/** Username check ends here **/
```

The 2nd check block is same as the previous one but just for the password input

```C#
/** Password check begins here **/
int password_index = 0;
while (password_index < this.password.Length && !is_valid) {
    if (this.password[password_index] < '0' || this.password[password_index] > '9') {
        bool is_allowed = false;
        for (int i = 0; i < allowed_characters.Length; i++) {
            if (this.password[password_index] == allowed_characters[i]) {
                is_allowed = true;
                break;
            }
        }
        if (is_allowed) {
            is_valid = false;
        }
    }
    password_index++;
}
if (!is_valid) {
    new Form2("Password contains wrong characters.").Show();
    return false;
}
/** Password check ends here **/
```

The 3rd check is where the main logic is. This block initializes two `Random` generators, one with the length of username another with length of password.
Once done, two numbers are generated from each of those generator objects. These are then used to manipute the username and password respectively to generate new strings.
If both the generated strings match then the checks are successful and the function returns true. Otherwise a failure message is shown.

```C#
// If these initializations are done with then same number, then both the generator objects will return the same sequence of numbers
Random random = new Random(this.username.Length);
Random random2 = new Random(this.password.Length);
int num4 = random.Next();
int num5 = random2.Next();
string username_generated = "";
string password_generated = "";

for (int j = 0; j < this.username.Length; j++) {
    username_generated += (char)((int)this.username[j] + (num4 % 2));
}

for (int k = 0; k < this.password.Length; k++) {
    password_generated += (char)((int)this.password[k] + (num5 % 2));
}

int smallest_length = (username_generated.Length < password_generated.Length) ? username_generated.Length : password_generated.Length;

for (int current_index = 0; current_index < smallest_length && is_valid; current_index++) {
    if (username_generated[current_index] != password_generated[current_index]) {
        is_valid = false;
    }
}

if (!is_valid) {
    new Form2("The random generated credentials don't match.").Show();
    return false;
}
```

Our goal is to generate the same strings during this check and for that we would need someway to gurantee the same numbers are generated by both random number generators.
Now one thing that needs to be remembered that the `Random` class uses a pseudo-random number generator using the _Donald E. Knuth's subtractive random number generator algorithm_.
**The problem with this algorithm is that if the generator is feed with same seed value, then the algorithm return same values again and again**.
This can be exploited to break this program. We need to ensure that `num4` and `num5` have same value and this can be done when `random` and `random2` are seeded with same value.
I.e, this mean in order to control the generation of the strings, we need to pass same strings as both username and password.
I've return a same script that will randomly generate a small string that would generate a string based on the whitelisted characters which can be used as username and password combination

```python
from random import choice
from random import randint

allowed_characters = ['a', 'b', 'd', 'e', 'f', 'i', 'm', 'o', 'p', 'q', 's', 'x', 'w', 'z']
length = randint(8, 20)     # The password needs to atleast 8 characters long
string = ""

for _ in range(length):
    string += choice(allowed_characters)


print(f"Use string {string} for both username and password")

```
