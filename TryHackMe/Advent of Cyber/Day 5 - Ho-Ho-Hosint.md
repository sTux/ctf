﻿# Day 5 - Ho-Ho-Hosint
Very first OSINT challenge, really enjoyed...

## Track her down
Before beginning any of these challenges, I needed to figure out a source from where I can start collecting information about her. But all that was given was a image. Very well **exiftool** to the rescue. 

![exiftool output](exiftool%20output.png)
 
 Here among all the other EXIF data, if a creator data, which doesn't match with the name of our target,  but is very similar. Noting it down

## Got yaaa
The creator name that was noted down, searched it in Google and I'm greeted with a Twitter profile the first hit. And that's the jackpot. The account answer what's the birthday of our target, her current occupation, her phone's make and also point to a site, which look very much look a personal blog. **_Let's go down the rabbit hole_**

## Who's the woman?
Now that I've tracked down her personal/professional blog/portfolio, I tried the next question. That was bump. Will get back after some time. The next and last task given was to identify the woman in the target's blog/portfolio. _Looks familiar_ maybe Google Image Search can help. Copied the URL to the image and pasted it in the Google Image Search querybox, and voila, recognized.

## But where did it all started?
Now that even the last question has been done, only one question remains, when did she started? And I was stuck here. Poked around her site to find any dates or time of sorts. Noting. So I decided to look at hint. **Ahhhh!! Wayback Machine**

> Written with [StackEdit](https://stackedit.io/).
