﻿# Day 2 - Arctic Forum
In this challenge, we were made familiar with the concept of web sub-directory  enumeration and brute-forcing to find hidden web sub-directories using tools like DirBuster and gobuster. Although the walk-through of this challenge ask us use DirBuster, I used **gobuster** in `dir` mode to enumerate the sub-directory using the _provided wordlist_.

## Task 1 - Finding the hidden admin page
With a enumeration task, the hint was quite clear that this task will be enumeration task. After installing **gobuster** and the **DirBuster wordlist**, I issued this command to enumerate all the web sub-directories in the website
```bash
gobuster dir -u http://10.10.2.13:3000 -w wordlist/dirbuster/directory-list-2.3-medium.txt
```
This was the output, that I found

![gobuster Output](gobuster output.png)

As you can see, there are quite a few sub-directories that returned 302 and few which returned 200. Of all the sub-directories that returned a status code of 200, the one that strikes out most is the `/sysadmin` sub-directory. So I first went to that sub-directory it self [**10.10.2.13:3000/sysadmin**](http://10.10.2.13:300/sysadmin). And where was I presented with the admin login. This seems to the hidden page the task was referring.
>N.B: As you can see I interrupted dirbuster before the entire scan was over. And the finding of the page at first try was a lucky guess

## Task 2 - Finding out the password
Although we passed the barrier of uncovering the hidden admin panel, I was far from getting access to it. After guessing a few common combination, I decided to look into the source and the link that caught my eye was a comment saying that the design was available on github under some _Arctic Digital Design_.
This begin a enumeration task, I decided to google the name with _github_ suffixed to it. And luckily the first hit was  a [github repo](https://github.com/ashu-savani/arctic-digital-design) that contains the login credentials for the admin panel. Using that I tried to login into the `/sysadmin` login page and I was given access to the admin panel, just like that.

## Task 3 - Secret Ingredient
A quick look into the admin panel, and we will find a form to add users and an advertisement of sorts for a BYOE "partay". Sounds fun!! Also the last task seems to be interested in what the organizers of the "partay" wanted people to bring on their own. It being `Eggnog`, I tried it as the answer and voila correct and complete.

> Written with [StackEdit](https://stackedit.io/).
