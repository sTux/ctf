import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;


class Solver {
    private static byte[] encrypt(byte[] plain, String key) {
        byte[] result = null;

        try {
            Key keySpec = new SecretKeySpec(key.getBytes(), "DES");
            Cipher des = Cipher.getInstance("DES");
            des.init(Cipher.ENCRYPT_MODE, keySpec);
            result = des.doFinal(plain);
        } catch (Exception ex) {
            System.out.println("Error in encryption");
            System.exit(1);
        }

        return result;
    }


    public static void main(String[] args) {
        byte[] plaintext = new byte[] { 53, 111, 49, 118, 51, 100 };
        int magic_value = ((1337 * 32) / (16 * 16));

        // Round 1
        byte[] result1 = encrypt(plaintext, "13377331");

        // Round 2
        byte[] result2 = encrypt(result1, "13248657");

        StringBuilder builder = new StringBuilder();
        for (byte j: plaintext)
            builder.append((char)j);
        System.out.println("name = " + builder.toString());
        
        System.out.print("serial = ");
        for (byte i: result2) {
            i += magic_value;
            System.out.printf("%d ", i);
        }
        System.out.println("");
    }
}
