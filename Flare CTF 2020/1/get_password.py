# This is the password "decryption" and checking function

def password_check():
    altered_key = 'hiptu'
    key = ''.join([chr(ord(x) - 1) for x in altered_key])
    return key


if __name__ == "__main__":
    print(password_check())
