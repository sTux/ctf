---
title: PS Eclipse
url: https://tryhackme.com/room/posheclipse
---

## Question 1
Search for Event ID 11 and for files ending with exe extension.
Filter out logs for Windows Defender, OneDrive, Google Chrome updates and Windows updates.
We can see one filename that was created by powershell in Windows\TEMP. Another file with same name can be seen created by svchost.exe in System32\Tasks.
```
EventCode="11" TargetFilename="*exe" Image!="*Windows Defender\\*" Image!="*OneDrive*" Image!="*Google\\Update*" Image!="C:\\Windows\\SoftwareDistribution\\*" | table TargetFilename, Image
```
ref: https://answers.microsoft.com/en-us/windows/forum/all/what-is-the-difference-between-temp-folder-and-the/c4bfc690-d6e1-49a7-8ef5-fa4014f350cc


## Question 2
We know that powershell was used to drop the file on the system, thus we can check all event logs for powershell.exe process creation using the PID.
After finding the command, we need to decode the base64 command

EventCode="1" ProcessId=9412

> While the above way is how you find the log of how the powershell command was invoked, the question only needs the hostname
> EventCode="22" ProcessId=9412

## Question 3
Full path of powershell.exe


## Question 4
The base64 command blob contains a command to create scheduled task, or searching for schtasks.exe invocation
```
EventCode="1" CommandLine="*schtasks.exe*"
```

## Question 5
Similar to Q4, we can use logs to the command to find how the binary was executed using schtasks.exe
We know the service will run as SYSTEM, to find the full user we can find any process creation for the binary
```
EventCode="1" Image="C:\\Windows\\Temp\\OUTSTANDING_GUTTER.exe"
```

## Question 6
Similar to Q2, we need to find the domain the process connected. We can search in the DNS logs for the process
```
EventCode="22" Image="C:\\Windows\\Temp\\OUTSTANDING_GUTTER.exe"| top limit=20 QueryName
```

## Question 7
We know where the binary was dropped and also that powershell.exe was used to download, so search for the powershell script using
```
EventCode="11" Image="C:\\Windows\\System32\\WindowsPowerShell\\v1\.0\\powershell\.exe" TargetFilename="C:\\Windows\\Temp\\*" TargetFilename="*ps1"
```

## Question 8
I couldn't find any logs for the script execution but searching for the script name, a file delete log can be seen. Get hash for that one

## Question 9 + 10
Now that we know the infection was for BlackSun ransomware, we can just search for all the files created with "BlackSun" string present in them.
```
EventCode="11" TargetFilename="*BlackSun*" | table TargetFilename
```