﻿# Day 8 - SUID Shenanigans
This was the first Linux privilege escalation challenge, something I struggle a lot with in-spite of using using Linux for 3+ years :3
Let's get going.

## Finding the insertion point
I looked at the write-up to solve this one. Also I learned something about how **nmap** scans for ports. This challenge is basically a simple **nmap _port scan_** challenge. But with a catch, i.e _SSH service_  is running on a obscure port. I initially tried a _Connnect Scan_, with _OS discovery_ turned on, by
```bash
nmap -sV -sT -sC -O 10.10.124.113
```
But the write-up suggested a simple _Regular Scan_
```bash
nmap -p - -v 10.10.124.113
```
Now you may ask, Why the _Connect Scan_ couldn't find the port? The reason is the target is `65534`. **nmap** by default scans for open 1000 ports. That's why the initial _Connect Scan_ couldn't find the port. To make **nmap** scan all the ports, we need to use the _ports_ options like `-p -`. This makes **nmap** scan for all the ports. Now moving on....

## Invading Igor's home
This was quite a sneaky challenge. I was asked to find a way to read the _/home/igor/flag1.txt_. Well, of course now I need to escalate to Igor's privilege or higher. So I started enumerating the system using `find` utility.

It took quite sometime and a quick gloss over the supporting materials, to come up with the idea of checking the privilege level of `find` itself. Turns out it runs with _igor_ privilege. Very well, I'll just use `find` to read the file
```bash
find / -user igor -perm -4000 -exec cat /home/igor/flag1.txt \; 2> /dev/null
```

## Finishing off
And now the last one, A extremely pointless challenge. This challenge is basically a system enumeration challenge to find suspicious SUID binaries. I ran 
```
find / -user roor -perm -4000 -exec cat ls -l \; 2> /dev/null
```
Ran this, and a suspicious binary popped up in the enumeration result. Ran that and basically was asked for a entire shell command. Gave
```
cat /root/flag2.txt
```
as the input and got the second and last flag xD. Pointless challenge!!
> Written with [StackEdit](https://stackedit.io/app).
