# NeverLAN CTF Web


## Problem Statment
if you can't find it you're not looking hard enough

https://challenges.neverlanctf.com:1165/hello.html


## Solution
In challenge the authors hid the flag in plainsight using the `<span>` tag. The tag uses a white colour for the text and is just invisible with thr white background. Once we see the background, we can find the flag
