﻿# Day 15 - LFI
This challenge was my first introduction to _Local File Inclusion_ vulns.

## Where's Charlie going?
Question so easy it took me some time to figure it out where it was! xD

## Identifying the problem
Now that's out of the way, let's look at the sources of the page, especially the JS code that runs on this site. Immediatly one function catches my eye, `getFile()`. Making sense of the source reveals, that it talks to some API to fetch some contents stored on the server. And looking at how the function is getting called, it looks like `getFile()` uses a API that basically reads from a file that was passed to it.
```javascript
 function getNote(note, id) {
        const url = '/get-file/' + note.replace(/\//g, '%2f')
        $.getJSON(url,  function(data) {
          document.querySelector(id).innerHTML = data.info.replace(/(?:\r\n|\r|\n)/g, '<br>');
        })
      }
      // getNote('server.js', '#note-1')
      getNote('views/notes/note1.txt', '#note-1')
      getNote('views/notes/note2.txt', '#note-2')
      getNote('views/notes/note3.txt', '#note-3')
```
This is very well confirmed that this API suffers from LFI by doing this
```javascript
getNote('server.js', '#note-1')
// getNote('views/notes/note1.txt', '#note-1')
```
This will make the API read the contents of **server.js**, the webapp file that run on the backend. That's the identified our LFI vuln.

## Getting the secrets
Now that I have identified our LFI vuln, let's see if I can use. I open up the _Developer Tools -> Console_ to get a JS console. Now I did this
```javascript
getNote("/etc/shadow", "#note-1")
```
Andjust like that I got the dump of the shadow file. Skimmed through to it to find _Charlies_ entry. Found it. Copied it to a file called _charlie.hash_ and ran `john` using the _rockyou.txt_ wordlist. Got the password.

## Let's SSH
Now that I have _Charlies_ password, let's use it to **SSH** into the server as _charlie_. Did that and dropped into a shell for _charie_. A **ls** reveals that the _user.txt_ is in the directory. Read it and submit the flag
> Written with [StackEdit](https://stackedit.io/app).
