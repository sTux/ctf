# Leviathan Level 5 → Level 6


This level the binary read from the file **/tmp/file.log**, puts it stdout and deletes the file.
So to get the password of **leviathan6**, we can create a symbolic link of `/etc/leviathan_pass/leviathan6` to `/tmp/file.log`. Once done run the binary and get the password
