# Leviathan Level 0 → Level 1


This level was basic, as expected. Once we login using `ssh` and list all the directories, it becomes clear out target is in **.backup/**, which contains only one **.html** file.
So to extract the password from the html file, we can use `grep -i password bookmarks.html` and we will get the password for level 2.


Password for Level 2 -> rioGegei8m
