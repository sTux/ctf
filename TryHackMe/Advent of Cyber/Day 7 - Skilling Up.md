﻿# Day 7 - Skilling Up
This was a simple challenge that introduced to **nmap** port scanning tool. All I needed to do was scan the IP and all the information is displayed. When I did this, I needed the `-Pn` option, this was what I used
```bash
nmap -sC -sT -sV -O -Pn 10.10.91.31
```


> Written with [StackEdit](https://stackedit.io/app).
