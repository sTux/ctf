# leg


## Challenge Resources
C Source: [http://pwnable.kr/bin/leg.c](http://pwnable.kr/bin/leg.c) <br/>
Dumped ARM assembly: [http://pwnable.kr/bin/leg.asm](http://pwnable.kr/bin/leg.asm) <br/>
Url: leg@pwnable.kr <br/>
Port: 2222 <br/>
Password: guest <br/>


## Writeup
This challenge required us to guess the output of the functions `key1()`, `key2()` and `key3()` functions. The function `key1()` is very simple. It basically takes returns the value of the program counter register `pc`, which is *0x8ce0*.
The function `key3()` is also pretty simple, which returns the value of link register `lr` which in turn is the address where the function would return. The value is the *0x8d80* in the address in the main function where the function returns to.
The function `key2()` is the tricky of three. We can see at address *0x8cfc* the value of register `pc` + 1 moved to register `r6` and a branch is made on that address, which basically jumps to the next instruction. On there, the value of `pc` is copied to `r3` and 4 is added, which is address *0x8d10*. The address in `r3` is pushed onto the stack and then popped into `pc`, which makes the code jump to *0x8d10*, the value returned to main.

Once we have the return values of `key1()`, `key2()` and `key3()` we have the key and inputing it gives the flag
