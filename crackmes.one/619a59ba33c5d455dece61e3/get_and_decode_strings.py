import base64

strings = []

with open("crackmes.one/619a59ba33c5d455dece61e3/20$ CrackMe LOL.resource") as string_resources:
    current_string = ""
    for i in string_resources.read():
        if i in "=ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/":
            current_string += i
        else:
            if current_string:
                decoded_string = base64.b64decode(current_string)
                print(decoded_string.decode())
            current_string = ""
