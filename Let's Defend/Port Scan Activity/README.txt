Load the packet and wait for packet loading to complete.

To try and find the scanner IP, we can use Statistics > IPv4 Statistics > Source and Destination Addresses.
Looking at the Source IPv4 Addresses, we can see that 10.42.42.253 send the most packets to. Thus it's the scanning IP.

Other IPs in capture are 10.42.42.56, 10.42.42.50, 10.42.42.25, 10.255.255.255 (broadcast for 10.0.0.0/8).

Open Statistics > IPv4 Statistics > Destinations and Ports for traffic breakdown via (IPv4, Port) combination.
Use "ip.dst_host!=10.42.42.253 && ip.src_host==10.42.42.253" to get all the IP and ports contacted by 10.42.42.253

Now we have to find the IPs, we use a simple fact.
If any of the scanned IP is avaliable, then it'll return atleast one ACK-SYN TCP from one of the open ports to 10.42.42.253.
If not, then all that will be sent to 10.42.42.253 will be a ACK-RST TCP packet.
We can find which IP is avaliable by checking if it responded to 10.42.42.253 with anything but a ACK-RST packet
Expression: "ip.src == <scanned IP> && ip.dst == 10.42.42.253 && tcp.flags.reset == 0"

To find the MAC address of the Apple device, examine the ethernet address to find the Apple device.
Or we can find all packets originating from an Apple device using "eth.src.oui_resolved contains Apple"

While looking about for the open IP, we can see a RPC packet and we can confirm the OS from that TCP stream