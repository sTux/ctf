using System;

namespace Crackme {
	internal class Checker {
		private string username = "";
		private string password = "";

		public Checker(string username, string password) {
			this.username = username;
			this.password = password;
		}

		public bool IsValid() {
			if (this.password.Length < 8) {
				new Form2("Weak password.").Show();
				return false;
			}

			bool is_valid = true;
			char[] allowed_characters = new char[] { 'a', 'b', 'd', 'e', 'f', 'i', 'm', 'o', 'p', 'q', 's', 'x', 'w', 'z' };
			
			/** Username check begins here **/
			this.username = this.username.ToLower();
			int username_index = 0;
			while (username_index < this.username.Length && is_valid) {
				if (this.username[username_index] < '0' || this.username[username_index] > '9') {
					bool is_allowed = false;
					int num2 = 0;
					while (num2 < allowed_characters.Length && !is_allowed) {
						if (this.username[username_index] == allowed_characters[num2]) {
							is_allowed = true;
						}
						num2++;
					}
					if (!is_allowed) {
						is_valid = false;
					}
				}
				username_index++;
			}
			if (!is_valid) {
				new Form2("Username contains wrong characters.").Show();
				return false;
			}
			/** Username check ends here **/

			/** Password check begins here **/
			int password_index = 0;
			while (password_index < this.password.Length && !is_valid) {
				if (this.password[password_index] < '0' || this.password[password_index] > '9') {
					bool is_allowed = false;
					for (int i = 0; i < allowed_characters.Length; i++) {
						if (this.password[password_index] == allowed_characters[i]) {
							is_allowed = true;
							break;
						}
					}
					if (is_allowed) {
						is_valid = false;
					}
				}
				password_index++;
			}
			if (!is_valid) {
				new Form2("Password contains wrong characters.").Show();
				return false;
			}
			/** Password check ends here **/

			// If these initializations are done with then same number, then both the generator objects will return the same sequence of numbers
			Random random = new Random(this.username.Length);
			Random random2 = new Random(this.password.Length);
			int num4 = random.Next();
			int num5 = random2.Next();
			string username_generated = "";
			string password_generated = "";
			
			for (int j = 0; j < this.username.Length; j++) {
				username_generated += (char)((int)this.username[j] + (num4 % 2));
			}

			for (int k = 0; k < this.password.Length; k++) {
				password_generated += (char)((int)this.password[k] + (num5 % 2));
			}

			int smallest_length = (username_generated.Length < password_generated.Length) ? username_generated.Length : password_generated.Length;
			
			for (int current_index = 0; current_index < smallest_length && is_valid; current_index++) {
				if (username_generated[current_index] != password_generated[current_index]) {
					is_valid = false;
				}
			}
			
			if (!is_valid) {
				new Form2("The random generated credentials don't match.").Show();
				return false;
			}
			return is_valid;
		}
	}
}
