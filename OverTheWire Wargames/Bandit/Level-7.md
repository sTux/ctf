# Bandir Level 7 -> Level 8

## Question
The password for the next level is stored in the file data.txt next to the word millionth


## Solution
This level required simple `grep` operation on the file. The exact command needed is `cat data.txt | grep millionth`
