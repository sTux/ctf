Looking at the questions, it's better to dump the list of processes and network connections in JSON Lines format
    volatility -r jsonl -f 20210430-Win10Home-20H2-64bit-memdump.mem "windows.pslist" | tee process.jsonl
    volatility -r jsonl -f 20210430-Win10Home-20H2-64bit-memdump.mem "windows.netscan" | tee network_connections.json

To find the time of acquisation, we need to look at the SystemTime
    volatility -f 20210430-Win10Home-20H2-64bit-memdump.mem "windows.info"

To find the sha256sum of the image
    sha256sum 20210430-Win10Home-20H2-64bit-memdump.mem

To find the pid of brave.exe we use the dumped jsonl
    cat process.jsonl | jq "select(.ImageFileName == \"brave.exe\") | .PID"

To find the number of established network connections at the time acquisation
    cat network_connections.json | jq "select(.State == \"ESTABLISHED\") | .Created" | wc -l

To find the FQDN that was connected by chrome.exe
    cat network_connections.json | jq -r 'select((.State == "ESTABLISHED") and (.Owner == "chrome.exe")) | .ForeignAddr' | nslookup

To find the MD5 of the PID 6988 image
    volatility -f 20210430-Win10Home-20H2-64bit-memdump.mem "windows.pslist" --pid 6988 --dump
    md5sum pid.6988.0x1c0000.dmp

Open the image in any hex-editor and jump to that spot

To get the creation time of powershell.exe
    cat process.jsonl | jq -r "select(.ImageFileName == \"powershell.exe\") | .CreateTime"

To find the last file that was opened in notepad, we can start seeing the commandline.
    cat process.jsonl | jq -r "select(.ImageFileName == \"notepad.exe\") | .PID"
    volatility -f 20210430-Win10Home-20H2-64bit-memdump.mem "windows.cmdline" --pid 2520

To find the accurate runtime of brave.exe, find the usage statistics from UserAssist registry.
    volatility -r jsonl -f 20210430-Win10Home-20H2-64bit-memdump.mem "windows.registry.userassist" | tee userassist_registry.jsonl
    cat userassist_registry.jsonl | jq -r '.__children | .[] | select(.Name == "Brave") | ."Time Focused"'

