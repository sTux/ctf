---
title: New Hire Old Artifacts
url: https://tryhackme.com/room/newhireoldartifacts
---

## Question 1
To find the password stealer process, we can start by looking at the events of process creations. Considering the executable is a stealer, it would be safe to assume that the sample will be started from %TEMP%. Following queries can be used to find all the processes and associted details
```
EventCode="1" source="WinEventLog:Microsoft-Windows-Sysmon/Operational"
```
To find the images most executed from %TEMP%
```
EventCode="1" source="WinEventLog:Microsoft-Windows-Sysmon/Operational" | search Image="*Temp*" | table Image | top Image
```

## Question 2
Now that we have the image, we can drill down and find the details parsed by Sysmon
```
EventCode="1" source="WinEventLog:Microsoft-Windows-Sysmon/Operational" | search Image="C:\\Users\\FINANC~1\\AppData\\Local\\Temp\\11111.exe"
```

## Question 3 (Took Hint)
We now know which process was used to get the login data from browser, now to find other mailicious binaries that we executed from the same folder (remember Windows short and long path format)
```
EventCode="1" source="WinEventLog:Microsoft-Windows-Sysmon/Operational" | search Image="C:\\Users\\FINANCE01\\AppData\\Local\\Temp\\*" | search Image!="*11111.exe"
```

## Question 4
Now we have identified another malicious process, let's see if it connected to any of it's C2 address. First, using the previous query get the SHA256 hash and search it in VT. Now use the following query 
```
EventCode="3" ProcessId="7296" DestinationIp!="127.0.0.1" | table DestinationIp | uniq
```
Only 1 IP address, have URL present.

## Question 5
Now that we know the PID of the process, we can search for all the registry modifications using the following query. We need to ignore registry modifications that are done by OS.
```
EventCode="13" ProcessId=7296 | table TargetObject
```

## Question 6 (Took Hint)
From the hint, we know that taskill was used, so we can search for "taskkill" in process creation logs. From there we can find all the process killed
```
EventCode="1"  Image="C:\\Windows\\SysWOW64\\taskkill.exe" | table CommandLine
```

## Question 7
Similar to Q6, we can look at process created by powershell.exe
```
EventCode="1" Image="C:\\Windows\\SysWOW64\\WindowsPowerShell\\v1.0\\powershell.exe" | table CommandLine
```
ref: https://powershell.one/wmi/root/microsoft/windows/defender/msft_mppreference

## Question 8
From the table to the we can get all the threat IDs

## Question 9
The logs for this process was not found in process creation but in Image loading.
```
EventCode="7" ImageLoaded="*AppData*"  NOT ImageLoaded="C:\\Users\\FINANC~1\\AppData\\Local\\Temp\\11111.exe" NOT ImageLoaded="C:\\Users\\FINANC~1\\AppData\\Local\\Temp\\IonicLarge.exe"
```

## Question 10
Now that we have the process, we can use the same logs to see what DLL it loads
```
EventCode="7" ImageLoaded="*AppData*" NOT ImageLoaded="C:\\Users\\Finance01\\AppData\\Roaming\\EasyCalc\\EasyCalc.exe" ProcessId=8120 | table ImageLoaded | sort ImageLoaded
```
