package com.lfalch.crackme1;

import java.awt.event.*;
import javax.swing.*;

final class VerifyButtonListener implements ActionListener
{
    private final MainPanel panel;
    
    VerifyButtonListener(final MainPanel mainPanel) {
        super();
        this.panel = mainPanel;
    }
    
    @Override
    public void actionPerformed(final ActionEvent actionEvent) {
        if (InputVerification.verify(this.panel.user_name_textbox.getText(), this.panel.user_password_textbox.getText())) {
            JOptionPane.showMessageDialog(
                    null, "You have entered a valid Serial!",
                    "CRACKED", JOptionPane.INFORMATION_MESSAGE
            );
            return;
        }
        JOptionPane.showMessageDialog(
                null, "The information that you've entered is invalid",
                "Invalid", JOptionPane.WARNING_MESSAGE
        );
    }
}
