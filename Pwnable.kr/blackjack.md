# blackjack


## Challenge Resources
Url: [nc pwnable.kr 9009](nc pwnable.kr 9009)
Source Code: http://cboard.cprogramming.com/c-programming/114023-simple-blackjack-program.html


## Writeup
This challenge was based on a implementation of the BlackJack game in C. The goal is to reach a million units of in-program currency to get the flag.
Unlike previous challenges, this challenge didn't required any fancy hacker skills but only required enough logic compitency in C to understand the bug.
Here's some part of the source-code that's was used to develop the exploit'
```c
        if(p>21) //If player total is over 21, loss
        {
            printf("\nWoah Buddy, You Went WAY over.\n");
            loss = loss+1;
            cash = cash - bet;         <--- If the player reaches more than 21, then this can be used to increase cash if the bet amount is negetive
            printf("\nYou have %d Wins and %d Losses. Awesome!\n", won, loss);
            dealer_total=0;
            askover();
        }

        ----------------------------------------------------------------------------------------------------------------------------------

        dealer();
        if(dealer_total==21) //Is dealer total is 21, loss
        {
            printf("\nDealer Has the Better Hand. You Lose.\n");
            loss = loss+1;
            cash = cash - bet;          <---- If the dealer scores more than 21 in the game, then this can be used to increase cash if the bet amount is negetive
            printf("\nYou have %d Wins and %d Losses. Awesome!\n", won, loss);
            dealer_total=0;
            askover();
        }

        ----------------------------------------------------------------------------------------------------------------------------------

        if(player_total<dealer_total) //If player's total is less than dealer's total, loss
        {
            printf("\nDealer Has the Better Hand. You Lose.\n");
            loss = loss+1;
            cash = cash - bet;          <---- If the player chooses to stay and then the dealer scores more than 21, then this can be used to increase case if the bet amount is negetive
            printf("\nYou have %d Wins and %d Losses. Awesome!\n", won, loss);
            dealer_total=0;
            askover();
        }
        ----------------------------------------------------------------------------------------------------------------------------------

        <!-- Probably hold the flag in the server -->
        void cash_test() //Test for if user has cash remaining in purse
        {
            if (cash <= 0) //Once user has zero remaining cash, game ends and prompts user to play again
            {
                printf("You Are Bankrupt. Game Over");
                cash = 500;
                askover();
            }
        } // End Function

        int betting() //Asks user amount to bet
        {
            printf("\n\nEnter Bet: $");
            scanf("%d", &bet);

            <!-- The check below ensures that no overdraft is made during betting, but doesn't prevents against negetive input -->
            if (bet > cash) //If player tries to bet more money than player has
            {
                printf("\nYou cannot bet more money than you have.");
                printf("\nEnter Bet: ");
                scanf("%d", &bet);
                return bet;
            }
            else return bet;
        } // End Function
```
As it can be seen in the above code snippets that we can input negetive numbers in the betting function and the program will happily accept it.
Now since the cash deduction logic is only activated if the player scores more than 21, or dealer scores 21, player stays and dealer scores more than player, i.e, if the player looses.
And can be easily done by staying for around 2 turns.