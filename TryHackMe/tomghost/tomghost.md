﻿# tomghost
**tomghost** is the most recent challenge box avaliable on **TryHackMe**. Here's how I completed it.

## Compromising the machine
First thing first, we need to scan the machine using **nmap** to gather information about all the open ports and running services running in the machine. I decided to go for a _TCP Connect Scan_, using `nmap -sT -sV -O 10.10.3.236`

 - `-sT`: Use _TCP Connet Scan_
 - `-sV`: Run nmap's default scripts
 - `-O`: Try and guess the running OS
The scan reported these useful services running/information :
- _Apache Tomcat v9.0.30_ running @ `8080`
- _Tomcat AJP Connector_ running @ `8009`
- _OpenSSH server_ running @ `22`
- _Linux Kernel v3.10 - v3.13_
A running web server could very well be our way inside the machine. Googling about _Apache Tomcat server v9.0.30_ and it's security advisories reveal that it suffers from a new vulnerability called `Ghostcat` (CVE-2020-1938 and CNVD-2020-10487
), that allows for _abitary read from other files_. Here is a great article from [Trend Micro](https://blog.trendmicro.com/trendlabs-security-intelligence/busting-ghostcat-an-analysis-of-the-apache-tomcat-vulnerability-cve-2020-1938-and-cnvd-2020-10487/).

So, the next order of business is to exploit the vulnerability. At first I looked in metasploit, the one shipped with Kali Linux, for an available exploit. Turns out no such exploit is available. So I googled for an exploit and found one at [Packet Strom](https://packetstormsecurity.com/files/156542/Apache-Tomcat-AJP-Ghostcat-File-Read-Inclusion.html)
> Remeber: This exploit is for Python 2

This exploit reads the **"WEB-INF/web.xml"** by default, and that's the file we need to login into this machine. After a succesful execution of the script, the output will give you the login credentials for user _skyfuck_.

## Obtaining user.txt
Now that we can ssh into the server and get a shell, we should start looking for the `user.txt` file. One way would be using the `find` utility, but I decided to look around the _/home_ directory before. Turns out we have another user called _merlin_, who is in _/etc/sudoers_ and has the file `user.txt`. Read it.

## Changing user
Now that we have found a user _merlin_ with sudo privileges, we would to escalate to it's privilege. Looking in our current user's home directory, we can two file:

- `credentials.pgp`: A PGP encrypted file that looks like it contains some kind of credentials
- `tryhackme.asc`: A public key file meant to be imported into a PGP program, here `GNU PGP`
Since I don't have much experience with `GNU PGP`, I refered to some resources online and the also the official write-up. I did what was suggested and used **john** to crack it, but importing the `tryhackme.asc` to a hash file using **pgp2john**.
I used **john** with the **rockyou** wordlist
```bash
john --wordlist rockyou.txt tryhackme.hash
```
It gave me the passcode, that can be used to import the public key and then decrypt the file. Doing so reveals the credentials for the user **"merlin"**.

## Escalating privileges
Now that we have logged into user **"merlin"**, we need to use it's `sudo` permission to escalate privileges to read the _root.txt_. My first trial was to see we are allowed to get a shell using `sudo -i`. Doing so will report that user **"merlin"** is not allowed to run _/bin/bash_. That's the tip. As I found out later and after lot of trials, user **"merlin"** is not allowed to run any commands on that system. This frustated me for a while and I had to go look around for ideas, since user **"merlin"** can'r un anything on that system and there's no interesting SUID binaries that can be used to get a shell or execute shell commands. I google for a bit and cam across this [_StackOverflow_ answer](https://askubuntu.com/a/950823).
Turns out we can list our environment using `sudo -l`. And within this environment listing, we can also see what commands we're allowed to run. Turns out only `/usr/bin/zip`, and that too without a password.
And that was it. From there it was a simple 
```bash
sudo zip -r root.zip /root
``` 
This would create a archive of the **root**'s home directory. The created archive has the _read permission enabled for everyone_. So we can then use **unzip** to decompress the archive
```bash
unzip root.zip
```
Since unzipping does not preserve the original permissions and ownership of the file, the _root.txt_ file from _/root_ is now readable. Read it to get the last flag
> IP given here will change

> Challenge notes avaliable here: [tomghost_notes.txt](tomghost_notes.txt)

> Written with [StackEdit](https://stackedit.io/).
