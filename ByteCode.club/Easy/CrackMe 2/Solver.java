import java.util.zip.CRC32;
import java.security.MessageDigest;


class Solver {
	public static void main(String[] args) {
		String hash_prefix = "vhly[FR]";
		String name = "hello";

		byte[] md5Digest = null;
		try {
			// I.I(47) = "MD5"
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			// I.I(51) = "vhly[FR]"
			md5.update(hash_prefix.getBytes());
			md5.update(name.getBytes());
			
			md5Digest = md5.digest();
		} catch (Exception e) {
		}

		CRC32 crc32 = new CRC32();
		crc32.update(md5Digest);

		long crc32Digest = crc32.getValue();

		System.out.printf("Name input: %s", name);
		System.out.println("Convert to Base 36: %l\n", crc32Digest);
	}
}
