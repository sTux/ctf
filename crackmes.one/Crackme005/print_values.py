buf = []
values = ""
enc_values = open("I.gif", "rb").read()

length = enc_values[0] << 16 | enc_values[1] << 8 | enc_values[2]
key = enc_values[2]

buf = [val ^ key for val in enc_values[3:]]
values = "".join(map(chr, buf))

print(values)
