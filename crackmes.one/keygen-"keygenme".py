def ascii_sum(nickname):
    ascii_sum = 0

    for i in nickname:
        ascii_sum += ord(i)

    return ascii_sum


def main():
    nickname = input("Enter the nickname: ")
    # In UNIX, the program name argument also includes the "path" from where the program was invoked. So we also need to take that into account
    program_name = "./keygenme"

    nickname_sum = ascii_sum(nickname)
    first_value = ord(nickname[0]) * 3
    xor_value = nickname_sum ^ first_value
    program_length = len(program_name)

    shifted_value = xor_value << (program_length & 0x1f)

    print(shifted_value)


if __name__ == '__main__':
    main()

