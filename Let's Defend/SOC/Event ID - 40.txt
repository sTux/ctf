Event ID: 40
Rule Name: SOC110 - Proxy - Cryptojacking Detected
Source Address: 172.16.17.47
Destination Address: 67.199.248.10
Destination URL: https://bit.ly/3hNuByx
Action: Allowed


This alert was triggered due to a bit.ly URL which is a URL shortner. Before we can investigate we need to uncover the destination.
We can do so but appending a "+" to the alert URL. This will show the destination URL without the need to visit the site.
We can see that we've been Rick-Rolled: https://bitly.com/3hNuByx+. We can say with full certainity that this is False Positive
