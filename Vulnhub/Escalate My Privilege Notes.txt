Scanned the network for hosts. Found the VM @ 192.168.1.203 (nmap -sP 192.168.1.202/24)
Scanned the VM for services. Found Apache, OpenSSH, rpcbind and nfs_acl running.
Enumerated the rpcbind service @ port 111. Found nfs running.
Enumerated the nfs service using nmap and showmount, found "/tmp" is avaliable is for share.

During nmap enumeration, /phpshell.php was reported to be running. Used it to get a shell for user apache
Using that user enumerated the system for other user, found one called armour.
Also enumerating /etc revealed /etc/shadow, /etc/passwd, /etc/exports as world readable.
Used /etc/shadow and /etc/passwd to get login imformation about root and armour.
Cross-checked nfs shares configuration

Decided to use a socat reverse shell to get a tty backed shell for a stable shell on my machine.
The shell was for the user apache. Navigated to the user armour home.
Found a file Credentials.txt that looked like the password for the user armour.
Used it to login to the user armour. `su armour`
Used `sudo -l` to see if the the user has sudo permissions and the check the user's sudo configs.
Found a number of programs can be run using sudo NOPASSWD. Among them in /bin/bash
Used `sudo bash` to launch a bash shell with root permissions. Navigated to root's home and got the flag
