MALWARE000 - ELF file, use string. Flag is in Base64
just_some_js - JSFuck language. Decoder https://www.dcode.fr/jsfuck-language
this_is_not_js - Brainfuck language. Interpreter https://code.golf/brainfuck#brainfuck
malware101 - ELF file, flag is instack strings. See script malware101.py
malware201 - ELF file, Python equivalent in malware201-encryption.py, decryption script in malware201-decryption.py
file.zip_broken - Messed up headers. Incorrect "Filename Length" section of the Local headers.