Download the file and use file to discover the type of file. Turns out a OOXML file.
Search for the file in VirusTotal, Any.Run and FileScan.IO
VirusTotal: https://www.virustotal.com/gui/file/7bcd31bd41686c32663c7cabf42b18c50399e3b3b4533fc2ff002d9f2e058813/detection
Any.Run: https://app.any.run/tasks/490790a1-81a9-4f5d-9424-b07ff4f396c1/
FileScan.IO: https://www.filescan.io/reports/7bcd31bd41686c32663c7cabf42b18c50399e3b3b4533fc2ff002d9f2e058813/b435489b-94d5-4782-9a86-e2f1373b8002/overview

We can get the creation date of the document from docProps/core.xml file or from LibreOffice Properties (MM/DD/YYYY format).
From the VirusTotal, we can see under what name BitDefender detects this maldoc
From the any.run execution, we can see that 2 VBS file and one WScript file, making a total of 3 dropped files.
From VirusTotal > Relations > Bundled Files, we can see only one .emf file. We can find SHA256 hash from there.
From FileScan.IO scan, we can the URL from Indicators of Compromise section.