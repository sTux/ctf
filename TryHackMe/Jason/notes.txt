Scan the box using nmap. Open are
    TCP port 22 for SSH
    TCP port 80 for HTTP server

Run gobuster to find directories but it didn't find anything
Opened the website and looked around a bit and used the site a bit. Found the cookie called session is set as Base64 encoded JSON file.
Since the room has deserialization as tag, let's try to exploit nodejs deserialization attacks
A sample payload was used to try out: {"email": "_$$ND_FUNC$$_function (){ return 'hi'; }()"} with BASE64 encoded
Set the value of session cookie and reload the site. It works!!

Now try and get reverse shell!! MSFVENOM shell doesn't work but this script generates a good reverse shell https://github.com/ajinabraham/Node.Js-Security-Course/blob/master/nodejsshell.py
Run it, put the payload in the test payload's function body and set the cookie. Reload to get reverse shell
Go to user home. Get user flag. Also noticed user can sudo
Use sudo -l to check present sudo config. /usr/bin/node can be run without password
Use method from here https://gtfobins.github.io/gtfobins/npm/
Got the root flag.