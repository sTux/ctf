﻿# Day 3 - Evil Elf
This level was less of a hacking level but more of a introduction to the wireshark and hashcat tools and how to use them on a toy example.

## Task 1 - Find the packet
Once we have downloaded the _pcap_ file, I opened it using Wireshark. The task asked me to find the destination IP address of packet number 998. Simple enough, just scroll down till I see that packet. Once the packet is found, click on it and inspect the IP header. The destination IP will be present there

![packet details](packet detail.png)

## Task 2 - What's in that packet
The second task asks for a Christmas List. Odd! I haven't seen any list uptil now. Maybe it has something to do with the packets. To find this out, I used to `Follow TCP Stream` functionality of Wireshark to follow the entire packet stream. And there it was. A output redirection to a file named `christmas_list`.
![packet data](packet data.png)

## Task 3 - Get that password
Now that I can see what the client-server communication was, I noticed that much of the data looks like the content of a Linux `/etc/shadow` file. All the entries looks like system users of a Ubuntu system except the last entry of _buddy_. That's a normal user. Copy that entry and put it a file. Now it's time to fire hashcat on that file. Using the **rockyou wordlist** supplied by the challenge, the password can be cracked in seconds
```bash
hashcat -m 1800 -O target_hashes ~/wordlist/rockyou.txt
```
![hashcat output](hashcat output.png)

> Written with [StackEdit](https://stackedit.io/).
