import java.security.MessageDigest;
import java.util.Base64;
import java.util.Date;


class Part1 {
    public static void main(String[] args) throws Exception {
        // Name I'll use for the serial generation
        String name = "cr@ck3r";

        Date current = new Date();
        int month = current.getMonth() + 1;
        int date = current.getDay();
        String str = "";
        MessageDigest md5HashInstance = MessageDigest.getInstance("MD5");

        switch (date % 5) {
            case 0:
                str = "Good Luck For Writer";
                break;
            case 1:
                str = "Foikd This is Very EasY?";
                break;
            case 2:
                str = "The Day is g\\ook/risse.x-> vhly";
                break;
            case 3:
                str = "No ONe Can sToP mE!ARe You";
                break;
            case 4:
                str = "Thank you for CraCK This :)";
                break;
        }
        md5HashInstance.update(str.getBytes());

        switch (month % 3) {
            case 0:
                str = "Email is What? \"vhly@163.com is Right!\",So YOu Can Only Make a KeyGen";
                break;
            case 1:
                str = "I love YoU , My Girls, HAHaaaaa!";
                break;
            case 2:
                str = "GivE a Right Reason, I Think this CrackMe 's Serial is Ofen change for A Name";
                break;
        }
        md5HashInstance.update(str.getBytes());

        byte[] nameArray = name.getBytes();
        char key = (char)( 32 + ((char)(date % 4)) );
        // Encrypt the name array
        for (int i=0; i<nameArray.length; i++)
            nameArray[i] = (byte) (nameArray[i] ^ key);
        md5HashInstance.update(nameArray);

        String base64 = Base64.getEncoder().encodeToString(md5HashInstance.digest());

        System.out.println("Serial is " + base64);
    }
}
