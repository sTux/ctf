Started the machine and started a nmap scan of the machine `nmap --script="vuln" -sC -sV -v`.
    Found OpenSSH server @ 22
    Found HTTP server @ 80
Started a nikto scan of the webserver. Found it's a apache server 2.4.18 on Ubuntu. Started running gobuster with SecList common.txt wordlist. Got hit: /content.
Visted http://10.10.137.242/content on browser. Saw it was running SweetRice CMS. Launched another gobuster enumeration session. Got hits: /as, /attachment, /inc
Since the running CMS is Sweetrice, I used `searchsploit sweetrice` to find exploits. Found one that site backup disclosure vulnerability. 
Used it to read the database dump of the site. Found the username and hashed passwords for what looks like the login for panel at /as. Using crackstation, recovered the password.
Now using the found credentials, logged into the CMS admin panel. Now that I can authenticate with the webserver, I tried to use the arbitary file upload exloit to upload php reverse shell.
After a lot of trying, found that .php extension is discarded but phtml is allowed. So I copied the reverse shell from Seclists/Web-Shells/laudanum-0.8/php/php-reverse-shell.php, changed extension to phtml and uploaded it.
Copied and visited the url given by the script and voila the reverse shell worked and gave me a foothold into the machine as user www-data.
Visted the /home directory first and saw the home for user it-guy. Listed his directory and found the user flag. Also a suspicious perl script called backup.pl
Before looked for anything else, I decided to stablise my shell by spawing a pty-backed shell using python3. Read the contents of the file of backup.pl and started a linpeas enumeration.
Found that the file that used to be executed using backup.pl is world writable. Also linpeas reported that the user www-data can run the command `/usr/bin/perl /home/itguy/backup.pl` with sudo.
So now I have a perl script that executes a shell script. I also have the permission to execute the perl script using sudo and I can write to the shell script.
So I used `echo "chmod +s /bin/bash" > /etc/copy.sh` to over-ride everything that was in the file and used `sudo /usr/bin/perl /home/itguy/backup.pl` to execute the perl script.
After that I the /bin/bash executable was marked as SUID. Now using `bash -p`, I started a bash shell preserving the SUID permission and I got the root shell.
Moved into /root and got the root flag
