let encoded_flag = "P1xNFigYIh0BGAofD1o5RSlXeRU2JiQQSSgCRAJdOw=="
let password = btoa("goldenticket")
let key = atob(encoded_flag);
let flag = "";

for (let i = 0; i < key.length; i++) {
    flag += String.fromCharCode(
        key.charCodeAt(i) ^ password.charCodeAt(i % password.length)
    )
}

console.log(flag)