﻿# Day 6 - Data Elf-iltration
A primer into date ex-filtration techniques and how to identify them. This challenge came with a `.pcap` packet capture file and a extremely helpful documentation about ex-filtration techniques, actually the first documentation I read completely. I used **Wireshark**, **fcrackzip**, **steghide** and **xxd** to solve the challenges

## Stolen via DNS
So according to the documentation, hackers like to abuse DNS queries to ex-filtrate data to their servers and this queries tend to have a long prefix in front of the site's domain name. So first I looked for that and two queries `A` queries came to attention having a strange long alpha-numeric prefix in front of the domain name. Let's copy the alphanumeric string and keep it in a file. And right after this I lost. I actually had no clue what I was going to do and I proceeded to the next challenge, only to comeback equally lost. One write-up actually pointed out it was a hexadecimal string and then with a [:facepalm:], I continued with using `xxd` to get the flag
```bash
echo "43616e64792043616e652053657269616c204e756d6265722038343931" | xxd -r -p
```
Let's move on...


## What's more in there?
After the DNS, I moved on to check the other traffic recorded in the file. Some grey-ed out parts looked like background noise traffic and inspecting gave no useful info. After those DNS records, there are records of more DNS and HTTP traffic, the DNS traffic is actually normal but the HTTP traffic is interesting. I used **Wireshark**'s **_Follow Traffic -> HTTP_** functionality to inspect the traffic. The first one look as a `GET` request-response pair, revealing that the user asked for a what appears to be a file listing containing a zip and a image. The consequent `GET` request-response pairs transfers the `.zip` and `.jpg` file to the user. Using **Wireshark**'s _**Export Object**_ functionality, I extracted the `HTML` file, the `.zip` file and the `.jpg` file.


## Unzipping the collection
Now that I have the `.zip` file, I tried to extract the file and it is password encrypted. Time to arm **fcrackzip**. Using the _rockyou.txt_ wordlist, I used 
```bash
fcrackzip -b -D -p rockyou.txt -v --method zip2 christmaslists.zip
```
and within seconds we have the password for the file. Then using **7z** to extract a host of `.txt` file that seems to be Christmas wishlists. Since the next challenge want to know what _Timmy_ wants for Christmas, I opened the file `christmaslisttimmy.txt` and copied what he wanted as flag. 2 down 1 to go


## Hidden under logo
Now for the last challenge we need to know what's hidden inside a certain file. Now since we have used DNS ex-filtration, and `.zip` cracking, I seems likely that our target is within the image. Using **steghide** I tried to extract the file
```bash
steghide extract -sf TryHackMe.jpg
```
On execution, **steghide** wanted a password for the file. This threw off the track for much time as I went hunting for the password, only to find that there was no password and simple enter will do the trick. Once I extracted the file, I opened the text file using **vim**. Looks like a poem, unrelated. But it also has _**RFC**_ number, let's try that and that would solve the last challenge
